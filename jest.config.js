module.exports = {
  maxWorkers: 2,
  // @see https://stackoverflow.com/a/44366115
  testEnvironment: 'node',
  testTimeout: parseInt(process.env.JEST_TEST_TIMEOUT || '5000', 10),
  collectCoverageFrom: [
    'src/**/*.{js,jsx,ts}',
    '!**/node_modules/**',
    '!src/routes/**',
    '!src/server.ts',
    '!src/sdk/__fixtures__/**',
    '!src/typings/**',
    /** @todo */
    '!src/App.ts',
    '!src/cli/**',
    '!src/sdk/projections/validator.ts',
    '!src/setup.ts',
    '!src/templates/**',
  ],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
  globalSetup: '<rootDir>/test/setup.ts',
  globalTeardown: '<rootDir>/test/teardown.ts',
  transform: {
    '^.+\\.(ts|js)$': 'babel-jest',
  },
  modulePathIgnorePatterns: ['<rootDir>/test/integration/projections'],
  watchPathIgnorePatterns: ['globalConfig'],
};

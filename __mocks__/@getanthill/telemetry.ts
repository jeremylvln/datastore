const fake = {
  logger: {
    debug: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
    removeAllListeners: jest.fn(),
    on: jest.fn(),
  },
  metrics: {
    createCounter: () => () => {},
    createHistogram: () => () => {},
    metric: () => {},
  },
};

function _create() {
  return fake;
}

const createCounter = jest.fn().mockImplementation(() => {
  return {
    add: jest.fn(),
  };
});

module.exports = {
  telemetry: _create(),
  create: _create,
  logger: fake.logger,
  start: jest.fn(),
  stop: jest.fn(),
  metric: jest.fn(),
  helpers: {
    getMeter() {},
    getMetricCounter(metric) {
      return createCounter();
    },
  },
  metrics: {
    createCounter: () => () => {},
    createHistogram: () => () => {},
    metric: () => {},
  },
  opentelemetry: {
    api: {
      metrics: {
        getMeter: jest.fn().mockImplementation(() => {
          return {
            createCounter,
          };
        }),
      },
    },
  },
};

import type { Services } from '../../typings';

import express from 'express';

import { stream, serverSentEvents } from './controllers';

function routes(services: Services) {
  const app = express.Router({ mergeParams: true });

  app
    .post('/:model/:source', stream(services))
    .get('/:model/:source/sse', serverSentEvents(services));

  return app;
}

export default routes;

import type { NextFunction, Request, Response } from 'express';
import type { Services } from '../../typings';

import { ok } from 'assert';

import { Datastore, Aggregator } from '../../sdk';

/**
 * @alpha
 *
 * Aggregation route
 *
 * @param services Services
 */
export function aggregate(services: Services) {
  return async (req: Request, res: Response, next: NextFunction) => {
    let aggregator: Aggregator;

    try {
      services.telemetry.logger.debug(
        '[aggregate] Datastores initialization...',
      );
      const datastores = new Map<string, Datastore>(
        services.config.datastores.map((c: any) => {
          ok(!!c.name, 'Missing Datastore configuration name');

          return [
            c.name as string,
            new Datastore({
              ...c.config,
              telemetry: services.telemetry,
              timeout: parseInt(req.header('timeout') || '30000', 10),
              token: req.header('Authorization') || '',
            }),
          ];
        }),
      );

      aggregator = new Aggregator(datastores);

      const pipeline = req.body;

      services.telemetry.logger.debug('[aggregate] Starting the aggregation', {
        steps_count: pipeline.length,
      });
      const aggregation = await aggregator.aggregate(pipeline);

      if (req.header('logs') === 'true') {
        // @ts-ignore
        res.body = { aggregation, logs: aggregator.logs };
      } else {
        // @ts-ignore
        res.body = aggregation;
      }

      // @ts-ignore
      res.json(res.body);
    } catch (err: any) {
      if (err === Aggregator.ERROR_INVALID_PIPELINE_DEFINITION) {
        err.status = 400;
        /* @ts-ignore */
        err.details = aggregator?.logs;

        return next(err);
      }

      if (Aggregator.ERRORS.includes(err)) {
        err.status = 422;
        /* @ts-ignore */
        err.details = aggregator?.logs;

        return next(err);
      }

      next(err);
    }
  };
}

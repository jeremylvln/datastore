import type { Services } from '../../typings';

import setup from '../../../test/setup';

import register from '.';

import fixtureUsers from '../../../test/fixtures/users';

describe('events', () => {
  let app;
  let services: Services;

  let models;

  beforeEach(async () => {
    app = await setup.build({
      features: {
        mqtt: {
          isEnabled: true,
        },
        amqp: {
          isEnabled: true,
        },
      },
    });

    models = await setup.initModels(app.services, [fixtureUsers]);

    try {
      const Users = models.getModel(fixtureUsers.name);

      await Promise.all([
        Users.getStatesCollection(Users.db(services.mongodb)).deleteMany({}),
        Users.getEventsCollection(Users.db(services.mongodb)).deleteMany({}),
        Users.getSnapshotsCollection(Users.db(services.mongodb)).deleteMany({}),
      ]);
    } catch (err) {
      // Possibly the User model does not exist
    }
  });

  afterEach(async () => {
    jest.restoreAllMocks();

    await app.services.amqp.end().catch((err) => console.error(err));
    await setup.teardownDb(app.services.mongodb);
  });

  describe('#functional', () => {
    it('allows to receive events', async () => {
      app.services.config.features.mqtt.isEnabled = false;

      await app.services.amqp.connect();
      await app.services.amqp.init();

      await register(app.services);

      const { amqp, models } = app.services;
      try {
        await amqp._channel.purgeQueue(amqp.config.queue.name);
      } catch (err) {
        // ...
      }
      const message = amqp.next('users/created/success', 1000);

      const email = `alice+${setup.uuid()}@doe.org`;
      await amqp.publish(
        'users/created',
        {
          firstname: 'Alice',
          email,
        },
        {
          headers: {
            authorization: 'token',
          },
        },
      );

      await message;

      const Users = models.getModel('users');

      const [alice] = await Users.find(app.services.mongodb, {
        firstname: 'Alice',
      }).toArray();

      expect(alice).toMatchObject({
        firstname: 'Alice',
        email,
        version: 0,
      });
    });
  });
});

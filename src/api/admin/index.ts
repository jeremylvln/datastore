import type { Services } from '../../typings';

import express from 'express';

import {
  authenticate,
  getTokensByRole,
  OpenAPIMiddleware,
} from '../middleware';

import {
  create,
  createModelIndexes,
  getGraph,
  getModels,
  getSchema,
  rotateEncryptionKeys,
  update,
} from './controllers';

function routes(services: Services, openApi?: OpenAPIMiddleware) {
  const { config, telemetry } = services;
  const app = express.Router({ mergeParams: true });

  app
    .get(
      '/',
      authenticate(getTokensByRole(config.security.tokens, 'read')),
      getModels(services),
    )
    .get(
      '/graph',
      authenticate(getTokensByRole(config.security.tokens, 'read')),
      getGraph(services),
    )
    .get(
      '/:model/schema',
      authenticate(getTokensByRole(config.security.tokens, 'read')),
      getSchema(services),
    );

  if (config.features.api.admin !== true) {
    return app;
  }

  telemetry.logger.info('[Feature] Admin API enabled');

  app
    .post(
      '/',
      authenticate(getTokensByRole(config.security.tokens, 'admin')),
      create(services, openApi),
    )
    .post(
      '/:model',
      authenticate(getTokensByRole(config.security.tokens, 'admin')),
      update(services, openApi),
    )
    .post(
      '/:model/indexes',
      authenticate(getTokensByRole(config.security.tokens, 'admin')),
      createModelIndexes(services),
    )
    .post(
      '/rotate/keys',
      authenticate(getTokensByRole(config.security.tokens, 'admin')),
      rotateEncryptionKeys(services),
    );

  return app;
}

export default routes;

import type { NextFunction, Request, Response } from 'express';
import type { Access, AccessLevel } from '../../typings';

export function getTokensByRole(config: Access[], role: string): Access[] {
  const levels: AccessLevel[] = ['read', 'decrypt', 'write', 'admin'];

  while (levels.length > 0) {
    if (role === levels[0]) {
      return config.filter((access) => levels.includes(access.level));
      // .map((access) => access.token);
    }

    levels.shift();
  }

  return [];
}

export function getAuthorizationToken(req: Request): string {
  return (
    req.cookies?.token ??
    req.header('Authorization') ??
    (req.query.token as string)
  );
}

export function isAuthorized(
  tokens: Access[],
  token: string,
): Access | undefined {
  return tokens.find((t) => t.token === token);
}

export function authenticate(tokens: Access[]) {
  return (req: Request, res: Response, next: NextFunction) => {
    // @ts-ignore
    if (res.body) {
      return next();
    }

    const token = getAuthorizationToken(req);

    if (token === req.query.token) {
      delete req.query.token;
    }

    if (!token) {
      return next({
        status: 401,
        message: 'Unauthenticated',
      });
    }

    const access = isAuthorized(tokens, token);

    if (access) {
      res.locals.id = access.id;
      res.locals.level = access.level;

      return next();
    }

    return next({
      status: 403,
      message: 'Unauthorized',
    });
  };
}

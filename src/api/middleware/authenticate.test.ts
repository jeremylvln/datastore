import { Access } from '../../typings';
import {
  authenticate,
  checkProcessingAuthorization,
  getTokensByRole,
} from './authenticate';

describe('middleware/authenticate + getTokensByRole', () => {
  describe('#authenticate', () => {
    it('returns a 401 Unauthenticated if no Authorization token is provided', () => {
      const middleware = authenticate([]);

      const next = jest.fn();
      const req = {
        header: jest.fn().mockReturnValue(null),
        query: {},
      };

      middleware(req, {}, next);

      expect(next).toHaveBeenCalledWith({
        message: 'Unauthenticated',
        status: 401,
      });
    });

    it('skips the authentication if a body is already attached to the response', () => {
      const middleware = authenticate([]);

      const next = jest.fn();
      const req = {
        header: jest.fn().mockReturnValue('token'),
        query: {},
      };

      middleware(
        req,
        {
          body: {
            status: true,
          },
        },
        next,
      );

      expect(next).toHaveBeenCalledWith();
    });

    it('returns a 403 Unauthorized if no Authorization token is provided', () => {
      const middleware = authenticate([]);

      const next = jest.fn();
      const req = {
        header: jest.fn().mockReturnValue('token'),
        query: {},
      };

      middleware(req, {}, next);

      expect(next).toHaveBeenCalledWith({
        message: 'Unauthorized',
        status: 403,
      });
    });

    it('calls next without error if token is valid through headers', () => {
      const middleware = authenticate([
        {
          id: 'read',
          level: 'read',
          token: 'token',
        },
      ]);

      const next = jest.fn();
      const req = {
        header: jest.fn().mockReturnValue('token'),
        query: {},
      };
      const res = {
        locals: {},
      };

      middleware(req, res, next);

      expect(next).toHaveBeenCalledWith();
    });

    it('calls next without error if token is valid through request query', () => {
      const middleware = authenticate([
        {
          id: 'read',
          level: 'read',
          token: 'token',
        },
      ]);

      const next = jest.fn();
      const req = {
        header: jest.fn(),
        query: {
          token: 'token',
        },
      };
      const res = {
        locals: {},
      };

      middleware(req, res, next);

      expect(next).toHaveBeenCalledWith();
    });
  });

  describe('#getTokensByRole', () => {
    const tokens: Access[] = [
      {
        id: 'read',
        level: 'read',
        token: 'read',
      },
      {
        id: 'decrypt',
        level: 'decrypt',
        token: 'decrypt',
      },
      {
        id: 'write',
        level: 'write',
        token: 'write',
      },
      {
        id: 'admin',
        level: 'admin',
        token: 'admin',
      },
    ];

    it('returns read tokens only', () => {
      expect(getTokensByRole(tokens, 'read')).toEqual([
        {
          id: 'read',
          level: 'read',
          token: 'read',
        },
        {
          id: 'decrypt',
          level: 'decrypt',
          token: 'decrypt',
        },
        {
          id: 'write',
          level: 'write',
          token: 'write',
        },
        {
          id: 'admin',
          level: 'admin',
          token: 'admin',
        },
      ]);
    });

    it('returns decrypt tokens only', () => {
      expect(getTokensByRole(tokens, 'decrypt')).toEqual([
        {
          id: 'decrypt',
          level: 'decrypt',
          token: 'decrypt',
        },
        {
          id: 'write',
          level: 'write',
          token: 'write',
        },
        {
          id: 'admin',
          level: 'admin',
          token: 'admin',
        },
      ]);
    });

    it('returns write tokens only', () => {
      expect(getTokensByRole(tokens, 'write')).toEqual([
        {
          id: 'write',
          level: 'write',
          token: 'write',
        },
        {
          id: 'admin',
          level: 'admin',
          token: 'admin',
        },
      ]);
    });

    it('returns admin tokens only', () => {
      expect(getTokensByRole(tokens, 'admin')).toEqual([
        {
          id: 'admin',
          level: 'admin',
          token: 'admin',
        },
      ]);
    });

    it('returns no token on invalid role', () => {
      expect(getTokensByRole(tokens, 'invalid')).toEqual([]);
    });
  });
});

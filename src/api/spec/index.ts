import type { Models } from '../../models';
import type { SpecFragment } from '../../typings';

import merge from 'lodash/merge';

import config from '../../config';

import components from './components';

import * as builder from './builder';

export const SPEC_FRAGMENT: SpecFragment = {
  openapi: '3.0.0',
  servers: config.openApi.spec.servers || [
    {
      description: 'API Server',
      url: '/api',
    },
  ],
  info: merge(
    {
      title: 'Datastore API',
      description: 'This API is allowing you to create new entities easily',
      version: '0.1.0',
      license: {
        name: 'MIT',
        url: 'https://opensource.org/licenses/MIT',
      },
    },
    config.openApi.spec.info,
  ),
  tags: [],
  components,
  paths: {},
};

export function build(models: Models) {
  return builder.build(SPEC_FRAGMENT, models);
}

import errors from './errors';

const components = {
  securitySchemes: {
    // bearerAuth: {
    //   type: 'http',
    //   scheme: 'bearer',
    //   bearerFormat: 'JWT',
    // },
    apiKey: {
      description: 'API Key',
      type: 'apiKey',
      in: 'header',
      name: 'Authorization',
    },
  },
  schemas: {
    error: {
      type: 'object',
      required: ['status', 'message'],
      properties: {
        status: {
          type: 'number',
          example: 400,
          description: 'Error code status',
        },
        message: {
          type: 'string',
          example: 'Bad Request',
          description: 'Error message',
        },
      },
    },
    object_id: {
      type: 'string',
      pattern: '^[0-9a-f]{24}$',
      description: 'An object id',
      example: '54759eb3c090d83494e2d804',
    },
    'service-id': {
      type: 'string',
      description: 'Service unique identifier',
      example: 'john-doe',
      minLength: 3,
    },
    'request-id': {
      type: 'string',
      description: 'Unique Request ID',
      example: '123e4567-e89b-12d3-a456-426655440000',
    },
    page: {
      type: 'integer',
      default: 0,
      description: 'Current page index',
    },
    page_size: {
      type: 'integer',
      default: 1000,
      description: 'Current results limit applied',
    },
  },
  parameters: {
    page: {
      name: 'page',
      in: 'query',
      schema: {
        $ref: '#/components/schemas/page',
      },
    },
    page_size: {
      name: 'page_size',
      in: 'query',
      schema: {
        $ref: '#/components/schemas/page_size',
      },
    },
  },
  requestBodies: {},
  headers: {
    'pagination-count': {
      description: 'Total number of metadatas matching the query',
      schema: {
        type: 'integer',
      },
    },
    'pagination-page': {
      description: 'Page index',
      schema: {
        type: 'integer',
        minimum: 0,
        default: 0,
      },
    },
    'pagination-size': {
      description: 'Maximum page size',
      schema: {
        type: 'integer',
        minimum: 0,
        maximum: 1000,
        default: 100,
      },
    },
    'request-id': {
      description: 'The unique RequestID',
      schema: {
        $ref: '#/components/schemas/request-id',
      },
    },
    'service-id': {
      description: 'The caller service id',
      schema: {
        $ref: '#/components/schemas/service-id',
      },
    },
  },
  examples: {},
  links: {},
  responses: errors,
};

export default components;

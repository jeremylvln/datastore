const errors = {
  default: {
    description: 'Operation error',
    content: {
      'application/json': {
        example: {
          status: 500,
          message: 'Internal Server Error',
        },
        schema: {
          $ref: '#/components/schemas/error',
        },
      },
    },
  },
  400: {
    description: 'Invalid Model',
    content: {
      'application/json': {
        example: {
          status: 400,
          message: 'Invalid Model',
        },
        schema: {
          $ref: '#/components/schemas/error',
        },
      },
    },
  },
  404: {
    description: 'Object not found',
    content: {
      'application/json': {
        example: {
          status: 404,
          message: 'Not Found',
        },
        schema: {
          $ref: '#/components/schemas/error',
        },
      },
    },
  },
  409: {
    description: 'Object is in conflict with another',
    content: {
      'application/json': {
        example: {
          status: 409,
          message: 'Conflict',
        },
        schema: {
          $ref: '#/components/schemas/error',
        },
      },
    },
  },
  422: {
    description: 'Entity must be created first',
    content: {
      'application/json': {
        example: {
          status: 422,
          message: 'Entity must be created first',
        },
        schema: {
          $ref: '#/components/schemas/error',
        },
      },
    },
  },
};

export default errors;

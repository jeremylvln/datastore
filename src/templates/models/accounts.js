const c = require('../../../dist/constants');

const MODEL_DATABASE = 'datastore';
const MODEL_NAME = 'accounts';
const CORRELATION_FIELD = 'account_id';

const is_enabled = c.COMPONENTS.is_enabled;
const tags = c.COMPONENT_TAGS;
const annotations = c.COMPONENT_ANNOTATIONS;

const email = {
  ...c.COMPONENT_EMAIL,
  description: 'Account email of the user',
  example: 'john@doe.org',
};

const auth = {
  type: 'string',
  description: 'Authentication token',
};

const is_email_verified = {
  ...c.COMPONENT_BOOLEAN,
  description: 'Is the email verified',
  example: false,
};

const email_verification_token = {
  ...c.COMPONENT_UUID_V4,
  description: 'Verification token for a newly registerd email',
};

const reset_password_uuid = {
  ...c.COMPONENT_UUID_V4,
  description: 'Unique ID to use to reset the password',
};

const reset_password_expires_by = {
  ...c.COMPONENT_DATE,
  description: 'Reset password request expiration date',
};

const properties = {
  is_enabled,
  tags,
  annotations,
  auth,
  // Email
  email,
  email_verification_token,
  is_email_verified,
  // Reset password
  reset_password_uuid,
  reset_password_expires_by,
};

module.exports = {
  is_enabled: true,
  db: MODEL_DATABASE,
  name: MODEL_NAME,
  correlation_field: CORRELATION_FIELD,
  encrypted_fields: ['email'],
  schema: {
    model: {
      properties,
    },
    events: {
      [c.EVENT_TYPE_CREATED]: {
        '0_0_0': {
          required: ['email', 'auth'],
          properties,
        },
      },
      [c.EVENT_TYPE_UPDATED]: {
        '0_0_0': {
          properties,
        },
      },
      [c.EVENT_TYPE_RESTORED]: {
        '0_0_0': {
          properties,
        },
      },
    },
  },
};

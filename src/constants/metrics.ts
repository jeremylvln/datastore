import { metrics } from '@getanthill/telemetry';

export const recordHttpRequestDuration = metrics.createHistogram(
  'http_request_duration_ms', // name
  'Duration of the Datastore HTTP requests in ms', // description
  [0.5, 1, 2, 3, 5, 10, 25, 50, 100, 250, 1000], // buckets
  'ms', // unit
);

export const incrementProcessStatus = metrics.createCounter<{
  state:
    | 'starting'
    | 'started'
    | 'stopping'
    | 'stopped'
    | 'crashing'
    | 'crashed';
}>('status', 'Application status');

export const incrementProcessing = metrics.createCounter<{
  state: 'request' | 'success' | 'error';
  model: string;
}>('processing', 'Processing started');

// API
// Models
export const incrementApiCreate = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_create', 'API create');
export const incrementApiUpdate = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_update', 'API update');
export const incrementApiPatch = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_patch', 'API patch');
export const incrementApiApply = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_apply', 'API apply');
export const incrementApiGet = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_get', 'API get');
export const incrementApiTimetravel = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_timetravel', 'API timetravel');
export const incrementApiRestore = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_restore', 'API restore');
export const incrementApiFind = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_find', 'API find');
export const incrementApiEvents = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_events', 'API events');
export const incrementApiSnapshot = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_snapshot', 'API snapshot');
export const incrementApiEncrypt = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_encrypt', 'API encrypt');
export const incrementApiDecrypt = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_decrypt', 'API decrypt');
export const incrementApiGraph = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_graph', 'API graph');
export const incrementApiArchive = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_archive', 'API archive');
export const incrementApiUnarchive = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_unarchive', 'API unarchive');
export const incrementApiDelete = metrics.createCounter<{
  state: 'request' | '200' | string;
  model: string;
}>('api_model_delete', 'API delete');

// Stream
export const incrementApiStreamSSE = metrics.createCounter<{
  state: 'created' | 'request' | '200' | string;
  model: string;
  source: 'events' | 'entities';
}>('api_stream_sse', 'API Stream SSE');
export const incrementApiStreamLegacy = metrics.createCounter<{
  state: 'created' | 'request' | '200' | string;
  model: string;
  source: 'events' | 'entities';
}>('api_stream_legacy', 'API Stream Legacy');

// Admin API
export const incrementApiAdminGetModels = metrics.createCounter<{}>(
  'api_admin_models_get',
  'Admin API GET models',
);
export const incrementApiAdminGetGraph = metrics.createCounter<{}>(
  'api_admin_graph_get',
  'Admin API GET graph',
);
export const incrementApiAdminGetSchema = metrics.createCounter<{}>(
  'api_admin_schema_get',
  'Admin API GET schema',
);
export const incrementApiAdminCreateModel = metrics.createCounter<{
  model: string;
}>('api_admin_models_create', 'Admin API create model');
export const incrementApiAdminUpdateModel = metrics.createCounter<{
  model: string;
}>('api_admin_models_update', 'Admin API update model');
export const incrementApiAdminUpdateModelIndexes = metrics.createCounter<{
  model: string;
}>('api_admin_models_update', 'Admin API update model indexes');
export const incrementApiAdminRotateEncryptionKeys = metrics.createCounter<{}>(
  'api_admin_keys_rotate',
  'Admin API rotate encryption keys',
);

// Deprecated
/**
 * @deprecated in favor of `status`
 */
export const incrementServerUp = metrics.createCounter<{}>(
  'server_up',
  'Server is up',
);

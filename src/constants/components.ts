export const COMPONENT_STRING = {
  type: 'string',
  description: 'A valid string',
};

export const COMPONENT_TAG_SNAKECASE = {
  type: 'string',
  description: 'Tag describe as `snake_case`',
  pattern: '^[0-9a-z_]+$',
};

export const COMPONENT_TAGS_SNAKECASE = {
  type: 'array',
  items: COMPONENT_TAG_SNAKECASE,
};

export const COMPONENT_BOOLEAN = {
  type: 'boolean',
  description: 'Boolean indicating the enabling state of the entity',
};

export const COMPONENT_INTEGER = {
  type: 'integer',
  description: 'Integer value',
};

export const COMPONENT_NUMBER = {
  type: 'number',
  description: 'number value',
};

export const COMPONENT_DATE = {
  type: 'string',
  description: 'ISO 8601 date format',
  example: '2019-01-01T00:00:00.000Z',
  format: 'date-time',
};

export const COMPONENT_PHONE_NUMBER = {
  type: 'string',
  description: 'Phone number',
  example: '+33623344556',
  minLength: 2,
  maxLength: 50,
  pattern: '^[+]?[(]?[0-9]{3}[)]?[-\\s.]?[0-9]{3}[-\\s.]?[0-9]{4,6}$',
};

export const COMPONENT_EMAIL = {
  type: 'string',
  description: 'Email address',
  example: 'john@doe.org',
  format: 'email',
};

export const COMPONENT_CORRELATION_ID = {
  type: 'string',
  description: 'Unique ID used as a correlation ID',
  example: '5fa962406a651140b5f9f4bf',
};

export const COMPONENT_UUID_V4 = {
  type: 'string',
  description: 'UUID V4',
  example: '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d',
  pattern: '^([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})?$',
};

export const COMPONENT_LOCALE = {
  type: 'string',
  description:
    'ISO 639-1 language identifier and optionaly a ISO 3166-1 alpha-2 region identifier',
  example: 'en',
  pattern: '[a-z]{2}(-[A-Z]{2})?',
};

export const COMPONENT_TIMEZONE = {
  type: 'string',
  description: 'Time zone name as defined in the tz database',
  example: 'Europe/Paris',
};

export const COMPONENT_URL = {
  type: 'string',
  description: 'Valid URL',
  example: 'https://example.com',
  pattern:
    'https?://(www.)?[-a-zA-Z0-9@:%._+~#=]{1,256}.[a-zA-Z0-9()]{1,6}([-a-zA-Z0-9()@:%_+.~#?&//=]*)',
};

export const COMPONENT_JSON_PATCH = {
  type: 'array',
  items: {
    type: 'object',
    required: ['op', 'path'],
    properties: {
      op: {
        type: 'string',
        enum: ['add', 'remove', 'replace', 'copy', 'move'],
        description: 'JSON PATCH Operation',
      },
      path: {
        type: 'string',
        description: 'JSON PATCH Operation path',
      },
    },
  },
};

/**
 * JSON Resume location schema in snake_case
 * @see https://jsonresume.org/schema/
 */
export const COMPONENT_LOCATION = {
  type: 'object',
  additionalProperties: false,
  properties: {
    address: {
      type: 'string',
      description:
        'To add multiple address lines, use \n. For example, 1234 Glücklichkeit Straße\nHinterhaus 5. Etage li.',
    },
    postal_code: {
      type: 'string',
    },
    city: {
      type: 'string',
    },
    country_code: {
      type: 'string',
      description: 'code as per ISO-3166-1 ALPHA-2, e.g. US, AU, IN',
    },
    region: {
      type: 'string',
      description:
        'The general region where you live. Can be a US state, or a province, for instance.',
    },
  },
};

export const COMPONENT_EVENT_TYPE = {
  type: 'string',
  description: 'Event unique type',
  example: 'CREATED',
};

export const COMPONENT_EVENT_TYPE_VERSION = {
  type: 'string',
  description: 'Event version following underscored semver',
  example: '1_0_0',
  // pattern:
  //   '^(0|[1-9]\\d*)_(0|[1-9]\\d*)_(0|[1-9]\\d*)(?:-((?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:_(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\\+([0-9a-zA-Z-]+(?:_[0-9a-zA-Z-]+)*))?$',
};

export const COMPONENT_EVENT_VERSION = {
  type: 'integer',
  description: 'Event history version',
  example: 0,
  minimum: 0,
};

export const COMPONENT_EVENT_PROPERTIES = {
  type: COMPONENT_EVENT_TYPE,
  v: COMPONENT_EVENT_TYPE_VERSION,
  version: COMPONENT_EVENT_VERSION,
  created_at: COMPONENT_DATE,
};

export const COMPONENT_STATE_PROPERTIES = {
  type: COMPONENT_EVENT_TYPE,
  version: COMPONENT_EVENT_VERSION,
  created_at: COMPONENT_DATE,
  updated_at: COMPONENT_DATE,
};

export const COMPONENT_EVENT_REQUIRED_FIELDS = ['type', 'v'];

export const COMPONENTS = {
  type: COMPONENT_EVENT_TYPE,
  v: COMPONENT_EVENT_TYPE_VERSION,
  version: COMPONENT_EVENT_VERSION,
  created_at: COMPONENT_DATE,
  updated_at: COMPONENT_DATE,
  is_enabled: {
    ...COMPONENT_BOOLEAN,
    description: 'Boolean indicating the enabling state of the entity',
  },
  is_readonly: {
    ...COMPONENT_BOOLEAN,
    description:
      'Boolean indicating that the entity can not be updated anymore',
  },
  is_archived: {
    ...COMPONENT_BOOLEAN,
    description: 'Boolean indicating that the entity has been archived',
  },
  is_deleted: {
    ...COMPONENT_BOOLEAN,
    description: 'Boolean indicating that the entity has been deleted',
  },
};

import { merge } from 'lodash';
import { MongoDbConnector } from '@getanthill/mongodb-connector';

import { Factory as ModelsFactory } from './Factory';

import setup from '../../test/setup';
import { Services } from '../typings';

describe('models/Factory', () => {
  const DEFAULT_SCHEMAS = {
    $id: 'events',
    components: {},
    model: {
      additionalProperties: true,
      properties: {
        firstname: {
          type: 'string',
        },
      },
    },
    events: {
      CREATED: {
        '0_0_0': {
          type: 'object',
          properties: {
            firstname: {
              type: 'string',
            },
          },
        },
      },
      UPDATED: {
        '0_0_0': {
          type: 'object',
          properties: {
            firstname: {
              type: 'string',
            },
          },
        },
      },
      AGE_UPDATED: {
        '0_0_0': {
          type: 'object',
          properties: {
            age: {
              type: 'integer',
            },
          },
        },
      },
    },
  };

  let schema;

  let app;
  let services: Services;
  let mongodb: MongoDbConnector;

  beforeAll(async () => {
    app = await setup.build();

    services = app.services;
    mongodb = services.mongodb;
  });

  beforeEach(async () => {
    schema = merge({}, DEFAULT_SCHEMAS);

    const constantDate = new Date('2020-11-10T00:00:00.000Z');

    // @ts-ignore
    global.Date = class extends Date {
      constructor() {
        super();

        return constantDate;
      }
    };
  });

  afterEach(async () => {
    jest.restoreAllMocks();
  });

  afterAll(async () => {
    await setup.teardownDb(mongodb);
  });

  it('returns a fully configured Model', async () => {
    const Users = ModelsFactory(
      {
        db: 'datastore',
        name: 'users',
        correlation_field: 'user_id',
        schema,
      },
      services,
    );

    const user = new Users(services);

    await user.create({
      firstname: 'John',
    });

    expect(user.state).toMatchObject({
      firstname: 'John',
      version: 0,
    });
  });
});

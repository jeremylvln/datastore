import vm from 'vm';

export const CACHE_HANDLERS = new Map<string, Function>();

export function handle(handler: Function, state: any, event: any) {
  if (typeof handler === 'function') {
    return handler(state, event);
  }

  let script = CACHE_HANDLERS.get(handler);

  if (script === undefined) {
    script = vm.compileFunction(handler, ['state', 'event']) as (
      ...args: any[]
    ) => any;

    CACHE_HANDLERS.set(handler, script);
  }

  return script(state, event);
}

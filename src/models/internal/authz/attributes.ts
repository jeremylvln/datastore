import type { ModelConfig } from '../../../typings';

import * as c from '../../../constants';

const MODEL_DATABASE = 'datastore';
const MODEL_NAME = 'attributes';
const MODEL_DESCRIPTION = 'ABAC authorization attribute';
const CORRELATION_FIELD = 'attribute_id';

const is_enabled = c.COMPONENTS.is_enabled;

const name = {
  ...c.COMPONENT_TAG_SNAKECASE,
  description: 'Attribute name',
};

const value = {
  description: 'Value associated to the attribute',
  anyOf: [
    { type: 'boolean' },
    { type: 'number' },
    { type: 'string' },
    { type: 'object' },
    { type: 'array' },
  ],
};

const description = {
  ...c.COMPONENT_STRING,
  description: 'Description of this conversation',
};

const scope = {
  ...c.COMPONENT_TAGS_SNAKECASE,
  description: 'Scope of the attribute',
  minItems: 1,
};

export const properties = {
  is_enabled,
  scope,
  name,
  value,
  description,
};

const modelConfig: ModelConfig = {
  is_enabled: true,
  db: MODEL_DATABASE,
  name: MODEL_NAME,
  description: MODEL_DESCRIPTION,
  correlation_field: CORRELATION_FIELD,
  schema: {
    model: {
      properties,
    },
    events: {
      [c.EVENT_TYPE_CREATED]: {
        '0_0_0': {
          required: ['scope', 'name', 'value'],
          properties,
        },
      },
      [c.EVENT_TYPE_UPDATED]: {
        '0_0_0': {
          properties: {
            is_enabled,
            value,
          },
        },
      },
      [c.EVENT_TYPE_RESTORED]: {
        '0_0_0': {
          properties,
        },
      },
    },
  },
  indexes: [
    {
      collection: MODEL_NAME,
      fields: { scope: 1, name: 1 },
      opts: {
        name: 'scope_1_name_1',
      },
    },
  ],
};

export default modelConfig;

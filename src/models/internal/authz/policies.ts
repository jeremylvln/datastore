import type { ModelConfig } from '../../../typings';

import * as c from '../../../constants';

const MODEL_DATABASE = 'datastore';
const MODEL_NAME = 'policies';
const MODEL_DESCRIPTION = 'ABAC authorization policies';
const CORRELATION_FIELD = 'policy_id';

const is_enabled = c.COMPONENTS.is_enabled;

const name = {
  ...c.COMPONENT_TAG_SNAKECASE,
  description: 'Name of the policy',
};

const description = {
  ...c.COMPONENT_STRING,
  description: 'Description of this policy',
};

const verb = {
  ...c.COMPONENT_STRING,
  description: 'Policy verb after rules validation',
  enum: ['allow', 'deny'],
};

const schema = {
  type: 'object',
  description: 'JSON Schema',
};

const rule = {
  type: 'object',
  required: ['name', 'action', 'subject', 'object', 'context'],
  properties: {
    name: {
      ...c.COMPONENT_TAG_SNAKECASE,
      description: 'Rule name',
    },
    description: {
      ...c.COMPONENT_STRING,
      description: 'Rule description',
    },
    subject: schema,
    action: schema,
    object: schema,
    context: schema,
  },
};

const rules = {
  type: 'array',
  description: 'List of rules',
  items: rule,
  minItems: 1,
};

const obligation = {
  type: 'object',
  required: ['name', 'type', 'source'],
  properties: {
    name: {
      ...c.COMPONENT_TAG_SNAKECASE,
      description: 'Obligation name',
    },
    description: {
      ...c.COMPONENT_STRING,
      description: 'Obligation description',
    },
    type: {
      type: 'string',
      description: 'Type of the obligation',
      enum: ['patch', 'pick'],
    },
    source: {
      type: 'string',
      description: 'Source of the request to apply the obligation on',
      enum: ['body', 'query', 'payload', 'headers'],
    },
    value: {
      description: 'Value associated to the obligation',
      anyOf: [
        { type: 'string' },
        { type: 'number' },
        { type: 'boolean' },
        { type: 'object' },
        { type: 'array' },
      ],
    },
  },
};

const obligations = {
  type: 'array',
  description: 'List of obligations',
  items: obligation,
};

const scope = {
  ...c.COMPONENT_TAGS_SNAKECASE,
  description: 'Scope of the policy',
  minItems: 1,
};

export const properties = {
  is_enabled,
  name,
  description,
  scope,
  verb,
  rules,
  obligations,
};

const modelConfig: ModelConfig = {
  is_enabled: true,
  is_internal: true,
  db: MODEL_DATABASE,
  name: MODEL_NAME,
  description: MODEL_DESCRIPTION,
  correlation_field: CORRELATION_FIELD,
  schema: {
    model: {
      properties,
    },
    events: {
      [c.EVENT_TYPE_CREATED]: {
        '0_0_0': {
          properties,
        },
      },
      [c.EVENT_TYPE_UPDATED]: {
        '0_0_0': {
          properties,
        },
      },
      [c.EVENT_TYPE_RESTORED]: {
        '0_0_0': {
          properties,
        },
      },
    },
  },
  indexes: [
    {
      collection: MODEL_NAME,
      fields: { scope: 1, is_enabled: 1 },
      opts: {
        name: 'scope_1_is_enabled_1',
      },
    },
  ],
};

export default modelConfig;

import type { ModelConfig } from '../../typings';

import * as c from '../../constants';

const MODEL_DATABASE = 'datastore';
const MODEL_NAME = '_logs';
const CORRELATION_FIELD = 'log_id';

const model = {
  ...c.COMPONENT_STRING,
  description: 'Model name linked to this log',
};

const correlation_id = {
  ...c.COMPONENT_CORRELATION_ID,
  description: 'Correlation id linked to this log',
};

const message = {
  ...c.COMPONENT_STRING,
};

const context = {
  type: 'object',
  description: 'Log context',
};

const level = {
  ...c.COMPONENT_INTEGER,
  description: 'Log level',
  minimum: 0,
  maximum: 100,
};

export const properties = {
  model,
  correlation_id,
  level,
  message,
  context,
};

const modelConfig: ModelConfig = {
  is_enabled: true,
  db: MODEL_DATABASE,
  name: MODEL_NAME,
  correlation_field: CORRELATION_FIELD,
  retry_duration: 5000,
  schema: {
    model: {
      additionalProperties: true,
      properties,
    },
    events: {
      [c.EVENT_TYPE_CREATED]: {
        '0_0_0': {
          additionalProperties: false,
          required: ['level', 'model', 'message'],
          properties,
        },
      },
      [c.EVENT_TYPE_UPDATED]: {
        '0_0_0': {
          additionalProperties: false,
          required: ['level', 'model', 'message'],
          properties,
        },
      },
      [c.EVENT_TYPE_RESTORED]: {
        '0_0_0': {
          properties,
        },
      },
      [c.EVENT_TYPE_ROLLBACKED]: {
        '0_0_0': {
          properties,
        },
      },
    },
  },
  indexes: [
    {
      collection: MODEL_NAME,
      fields: { model: 1, correlation_id: 1 },
      opts: { name: 'entity_type_1_correlation_id_1' },
    },
    {
      collection: MODEL_NAME,
      fields: { created_at: 1, level: 1 },
      opts: { name: 'created_at_1_level_1' },
    },
    {
      collection: MODEL_NAME,
      fields: { created_at: 1 },
      opts: { name: 'created_at_ttl', expireAfterSeconds: 10368000 }, // Remove the events after 3 months
    },
    {
      collection: `${MODEL_NAME}_events`,
      fields: { created_at: 1 },
      opts: { name: 'created_at_ttl', expireAfterSeconds: 2592000 }, // Remove the events after 30 days
    },
  ],
};

export default modelConfig;

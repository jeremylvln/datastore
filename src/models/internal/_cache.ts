import type { ModelConfig } from '../../typings';

import * as c from '../../constants';

const MODEL_DATABASE = 'datastore';
const MODEL_NAME = '_cache';
const CORRELATION_FIELD = 'cache_id';

const value = {
  type: 'object',
  description: 'Cache value',
};

const expires_by = {
  ...c.COMPONENT_DATE,
  description: 'Cache entry expiration date',
};

export const properties = {
  value,
  expires_by,
};

const modelConfig: ModelConfig = {
  is_enabled: true,
  db: MODEL_DATABASE,
  name: MODEL_NAME,
  correlation_field: CORRELATION_FIELD,
  retry_duration: 5000,
  schema: {
    model: {
      additionalProperties: true,
      properties,
    },
    events: {
      [c.EVENT_TYPE_CREATED]: {
        '0_0_0': {
          additionalProperties: false,
          required: ['value'],
          properties,
        },
      },
      [c.EVENT_TYPE_UPDATED]: {
        '0_0_0': {
          additionalProperties: false,
          required: ['value'],
          properties,
        },
      },
      [c.EVENT_TYPE_RESTORED]: {
        '0_0_0': {
          properties,
        },
      },
      [c.EVENT_TYPE_ROLLBACKED]: {
        '0_0_0': {
          properties,
        },
      },
    },
  },
  indexes: [
    {
      collection: MODEL_NAME,
      fields: { expires_by: 1 },
      // Remove the cache entry after 30 days
      opts: { name: 'expires_by_ttl', expireAfterSeconds: 3600 * 24 * 30 },
    },
    {
      collection: `${MODEL_NAME}_events`,
      fields: { expires_by: 1 },
      // Remove the events after 1 day
      opts: { name: 'expires_by_ttl', expireAfterSeconds: 3600 * 24 },
    },
  ],
};

export default modelConfig;

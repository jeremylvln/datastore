import buildJsonSchema, { replaceEncryptedFields, merge } from './schema';

import setup from '../../test/setup';
import { ModelConfig } from '../typings';

describe('schema', () => {
  const DEFAULT_MODEL_CONFIG: ModelConfig = Object.freeze({
    name: 'model',
    correlation_field: 'model_id',
    schema: {
      model: {
        type: 'object',
        properties: {
          firstname: {
            type: 'string',
            description: 'Firstname of the user',
          },
        },
      },
    },
  });

  let app;
  let services: Services;
  let mongodb: MongoDbConnector;

  beforeAll(async () => {
    app = await setup.build();

    services = app.services;
    mongodb = services.mongodb;
  });

  afterEach(async () => {
    jest.restoreAllMocks();
  });

  afterAll(async () => {
    await setup.teardownDb(mongodb);
  });

  describe('#buildJsonSchema', () => {
    it('returns a default valid schema by default', () => {
      const schema = buildJsonSchema(services, merge({}, DEFAULT_MODEL_CONFIG));

      expect(schema).toMatchSnapshot();
    });

    it('returns a valid schema with no default events', () => {
      const schema = buildJsonSchema(
        services,
        merge(
          {
            with_default_events: false,
            schema: {
              events: {
                CUSTOM: {
                  '0_0_0': {
                    type: 'object',
                    properties: {
                      custom_field: {
                        type: 'string',
                      },
                    },
                  },
                },
              },
            },
          },
          DEFAULT_MODEL_CONFIG,
        ),
      );

      expect(schema).toMatchSnapshot();
    });

    it('returns a valid schema with a specific correlation field', () => {
      const schema = buildJsonSchema(
        services,
        merge({}, DEFAULT_MODEL_CONFIG, {
          correlation_field: 'user_id',
        }),
      );

      expect(schema).toMatchSnapshot();
    });

    it('returns a valid schema without default events', () => {
      const schema = buildJsonSchema(
        services,
        merge({}, DEFAULT_MODEL_CONFIG, {
          with_default_events: false,
          schema: {
            events: {
              CREATED: {
                '0_0_0': {
                  type: 'object',
                  required: ['sentence'],
                  properties: {
                    sentence: {
                      type: 'string',
                      description: 'Say hello sentence',
                    },
                  },
                },
              },
            },
          },
        }),
      );

      expect(schema).toMatchSnapshot();
    });
  });

  it('lets the current schema unchanged if no encrypted field is defined', () => {
    const updatedModelConfig = replaceEncryptedFields({
      name: 'model',
      correlation_field: 'model_id',
      schema: {
        model: {},
      },
    });

    expect(updatedModelConfig).toEqual({
      name: 'model',
      correlation_field: 'model_id',
      schema: {
        model: {},
      },
    });
  });

  it('replaces the encrypted fields in model with 2 possibilities: one encrypted, a second one unchanged', () => {
    const updatedModelConfig = replaceEncryptedFields({
      name: 'model',
      correlation_field: 'model_id',
      encrypted_fields: ['firstname'],
      schema: {
        model: {
          type: 'object',
          properties: {
            firstname: {
              type: 'string',
              description: 'Firstname of the user',
            },
          },
        },
      },
    });

    expect(updatedModelConfig).toEqual({
      name: 'model',
      correlation_field: 'model_id',
      encrypted_fields: ['firstname'],
      schema: {
        model: {
          type: 'object',
          properties: {
            firstname: {
              description: 'Firstname of the user',
              anyOf: [
                {
                  type: 'object',
                  description: '`encrypted` Firstname of the user',
                  properties: {
                    hash: {
                      type: 'string',
                      description: '`sha512` value',
                      example:
                        'b8cccea15437aef415090bda6acb3b0ad3d4cf7d3e4cf816772e4b43e8f9d08af392bb98b8d532e07249f0d1304e6d65e007205c39913ee5db95578be398f4bd',
                    },
                    encrypted: {
                      type: 'string',
                      description: 'Encrypted value',
                      example:
                        '03d72e:1e9f1a960adfd0815530f7133c97dcd2:648b63622bb5bc3145de4d3f15fe05651ebdedbeab1d11986538214c4a25b834',
                    },
                  },
                },
                {
                  type: 'string',
                  description: 'Firstname of the user',
                },
              ],
            },
          },
        },
      },
    });
  });

  it('replaces the encrypted fields in events with 2 possibilities: one encrypted, a second one unchanged', () => {
    const updatedModelConfig = replaceEncryptedFields({
      name: 'model',
      correlation_field: 'model_id',
      encrypted_fields: ['firstname'],
      schema: {
        model: {
          type: 'object',
          properties: {
            firstname: {
              type: 'string',
              description: 'Firstname of the user',
            },
          },
        },
        events: {
          USER_CREATED: {
            '1_0_0': {
              type: 'object',
              properties: {
                firstname: {
                  type: 'string',
                  description: 'Firstname of the user',
                },
              },
            },
          },
        },
      },
    });

    expect(updatedModelConfig).toMatchSnapshot();
  });
});

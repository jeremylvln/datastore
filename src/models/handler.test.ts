import vm from 'vm';

import { handle, CACHE_HANDLERS } from './handler';

describe('models/handler', () => {
  const DEFAULT_HANDLER = `
    return [{
        ...state,
        count: state.count + event.count
      }];`;

  beforeEach(() => {
    CACHE_HANDLERS.clear();
  });

  it('executes code dynamically within the current context', async () => {
    expect(handle(DEFAULT_HANDLER, { count: 1 }, { count: 1 })).toEqual([
      { count: 2 },
    ]);
  });

  it('caches compiled handler after a first execution', async () => {
    expect(CACHE_HANDLERS.size).toEqual(0);

    handle(DEFAULT_HANDLER, { count: 1 }, { count: 1 });

    expect(CACHE_HANDLERS.size).toEqual(1);

    const spy = jest.spyOn(vm, 'compileFunction');
    handle(DEFAULT_HANDLER, { count: 1 }, { count: 1 });

    expect(CACHE_HANDLERS.size).toEqual(1);

    expect(spy).toHaveBeenCalledTimes(0);
  });

  it('throws an exception in case of error', async () => {
    let error;
    try {
      handle(DEFAULT_HANDLER, null, { count: 1 });
    } catch (err) {
      error = err;
    }

    expect(error.message.startsWith('Cannot read properties')).toEqual(true);
  });
});

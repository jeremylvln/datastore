import services, { getDatastores, build } from '.';

describe('services', () => {
  it('exports default services already configured', () => {
    expect(Object.keys(services).sort()).toEqual([
      'amqp',
      'api',
      'authz',
      'config',
      'datastores',
      'events',
      'metrics',
      'models',
      'mongodb',
      'mqtt',
      'signals',
      'telemetry',
    ]);
  });

  describe('#getDatastores', () => {
    it('returns no datastore client if none is defined', async () => {
      const datastores = await getDatastores({
        datastores: [],
      });

      expect(datastores.size).toEqual(0);
    });

    it('returns datastores instances for one configuration', async () => {
      const datastores = await getDatastores({
        datastores: [
          {
            name: 'source',
            config: {
              baseUrl: 'http://localhost:3210',
              token: 'token',
            },
          },
        ],
      });

      expect(datastores.size).toEqual(1);
      expect(datastores.get('source')!.config).toMatchObject({
        baseUrl: 'http://localhost:3210',
        token: 'token',
      });
    });

    it('throws an error if the Datastore configuration does not have a name', async () => {
      let error;
      try {
        const datastores = await getDatastores({
          datastores: [
            // @ts-ignore
            {
              config: {
                baseUrl: 'http://localhost:3001',
                token: 'token',
              },
            },
          ],
        });
      } catch (err) {
        error = err;
      }

      expect(error.message).toEqual('Missing Datastore configuration name');
    });
  });

  describe.skip('#logging', () => {
    let services;

    beforeEach(() => {
      services = build();

      // @ts-ignore
      services.models = {
        log: jest.fn(),
      };

      jest
        .spyOn(services.telemetry.logger.logger, 'info')
        .mockImplementation(() => null);
      services.telemetry.logger.config.level = 50;
    });

    afterEach(() => {
      jest.restoreAllMocks();
    });

    it('performs a noop if the  log does not have the `persist` flag to true in context', () => {
      services.telemetry.logger.info('test');

      expect(services.models.log).toHaveBeenCalledTimes(0);
    });

    it('performs a noop if the services does not have `models` defined', () => {
      services.models = undefined;

      services.telemetry.logger.info('test', {
        persist: true,
      });

      // What do I test?
    });

    it('stores the log within the internal log model', () => {
      services.telemetry.logger.info('test', {
        model: 'users',
        correlation_id: 'correlation_id',
        persist: true,
      });

      expect(services.models.log).toHaveBeenCalledWith(
        50,
        'users',
        'correlation_id',
        'test',
        {
          model: 'users',
          correlation_id: 'correlation_id',
          persist: true,
        },
      );
    });
  });
});

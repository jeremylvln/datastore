import type { Services } from '../typings';
import type { DatastoreConfig } from '../sdk/Datastore';

import { ok } from 'assert';
import { EventEmitter } from 'events';

import * as telemetry from '@getanthill/telemetry';
import { MongoDbConnector } from '@getanthill/mongodb-connector';
import MQTTClient from './mqtt';
import AMQPClient from './amqp';

import _config from '../config';
import * as metrics from '../constants/metrics';

import api from '../api';
import events from '../api/events';
import { init as initModels } from '../models';

import { Datastore } from '../sdk';
import Authz from './authz';

export function getDatastores(config: {
  datastores: {
    name: string;
    config: DatastoreConfig;
  }[];
}) {
  const datastores = new Map<string, Datastore>(
    config.datastores.map((c) => {
      ok(!!c.name, 'Missing Datastore configuration name');

      return [
        c.name,
        new Datastore({
          ...c.config,
          telemetry,
        }),
      ];
    }),
  );

  return datastores;
}

const services: Services = build(_config);

export function build(config: typeof _config = _config): Services {
  const mongodb = new MongoDbConnector(config.mongodb);

  const _services: Partial<Services> = {
    telemetry,
    config,
    api,
    events,
    mongodb,
    mqtt: new MQTTClient(config.mqtt, telemetry),
    amqp: new AMQPClient(config.amqp, telemetry),
    datastores: getDatastores(config),
    signals: new EventEmitter(),
    metrics,
  };

  _services.models = initModels(config.models, _services as Services);

  _services.authz = new Authz(config.authz, _services as Services);

  return _services as Services;
}

export default services;

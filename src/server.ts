import services from './services';

import App from './App';

const app = new App(services);

app
  .bind()
  .start()
  .catch((err) => services.telemetry?.logger.error('[start] Error', err));

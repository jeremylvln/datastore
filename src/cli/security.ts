import type { Services } from '../typings';

import { Command, Option } from 'commander';

import * as utils from './utils';

export function rotateKeys(services: Services) {
  return async (cmd: any) => {
    try {
      const datastore = services.datastores.get(cmd.datastore);

      if (!datastore) {
        return;
      }

      await datastore.rotateEncryptionKeys();

      utils.log(
        {
          requested: true,
        },
        cmd.format,
      );
    } catch (err: any) {
      if (err.response) {
        utils.log(err.response.data, cmd.format);

        return;
      }
      utils.log(err, cmd.format);
    }
  };
}

export default function register(services: Services, name = 'security') {
  const program = new Command(name);

  program.summary('Security commands');

  const c = program.command('keys:rotate');

  utils.addDatastoreOptions(c, services);

  c.addOption(
    new Option('--format <format>', 'Response format').choices([
      'json',
      'yaml',
    ]),
  )
    .description('Rotate encryption keys')
    .action(rotateKeys(services));

  return program;
}

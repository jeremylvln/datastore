import setup from '../../setup';

import Aggregator from './Aggregator';

describe('sdk/Aggregator', () => {
  let mongodb;
  let instance;

  let client;
  let aggregator;
  let uuid;

  beforeAll(async () => {
    [, mongodb, , , , client, , instance] = await setup.startApi({
      features: {
        api: {
          admin: true,
        },
      },
    });

    client.config.debug = true;

    uuid = setup.uuid();
  });

  beforeEach(() => {
    const datastores = new Map([['datastore', client]]);
    aggregator = new Aggregator(datastores);
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  afterAll(async () => {
    await setup.teardownDb(mongodb);

    await setup.stopApi(instance);
  });

  describe('retrieve nested objects', () => {
    beforeAll(async () => {
      await client.createModel({
        is_enabled: true,
        db: 'datastore',
        name: 'accounts',
        correlation_field: 'account_id',
        schema: {
          model: {
            type: 'object',
            properties: {
              firstname: { type: 'string' },
            },
          },
        },
      });

      await client.createModel({
        is_enabled: true,
        db: 'datastore',
        name: 'profiles',
        correlation_field: 'profile_id',
        schema: {
          model: {
            type: 'object',
            properties: {
              account_id: { type: 'string' },
              firstname: { type: 'string' },
            },
          },
        },
      });
    });

    it('returns all entities at once', async () => {
      const { data: account } = await client.create('accounts', {
        firstname: `Alice ${uuid}`,
      });

      const { data: profile } = await client.create('profiles', {
        account_id: account.account_id,
      });

      await client.update('accounts', account.account_id, {
        firstname: `Bernard ${uuid}`,
      });

      const data = await aggregator.aggregate([
        {
          type: 'fetch',
          datastore: 'datastore',
          model: 'accounts',
          destination: 'account',
          as_entity: true,
          query: {
            firstname: `Bernard ${uuid}`,
          },
        },
        {
          type: 'fetch',
          datastore: 'datastore',
          model: 'accounts',
          source: 'events',
          destination: 'account_events',
          map: [
            {
              from: 'account.account_id',
              to: 'account_id',
            },
          ],
        },
        {
          type: 'fetch',
          datastore: 'datastore',
          model: 'profiles',
          map: [
            {
              from: 'account.account_id',
              to: 'account_id',
            },
          ],
        },
      ]);

      expect(data).toMatchObject({
        account: {
          firstname: `Bernard ${uuid}`,
          version: 1,
        },
        account_events: [
          {
            type: 'CREATED',
            version: 0,
            firstname: `Alice ${uuid}`,
          },
          {
            type: 'UPDATED',
            version: 1,
            firstname: `Bernard ${uuid}`,
          },
        ],
        profiles: [
          {
            account_id: account.account_id,
            version: 0,
          },
        ],
      });
    });

    it('returns timetravel entities from fetch', async () => {
      const { data: account } = await client.create('accounts', {
        firstname: `Alice ${uuid}`,
      });

      const { data: profile } = await client.create('profiles', {
        account_id: account.account_id,
        firstname: `Alice ${uuid}`,
      });

      await client.update('accounts', account.account_id, {
        firstname: `Bernard ${uuid}`,
      });

      await client.update('profiles', profile.profile_id, {
        firstname: `Bernard ${uuid}`,
      });

      const data = await aggregator.aggregate([
        {
          type: 'fetch',
          datastore: 'datastore',
          model: 'profiles',
          source: 'events',
          destination: 'entity',
          as_entity: true,
          query: {
            type: 'CREATED',
            firstname: `Alice ${uuid}`,
          },
        },
        {
          type: 'fetch',
          datastore: 'datastore',
          model: 'accounts',
          source: 'entities',
          destination: 'account',
          as_entity: true,
          // timetravel
          timetravel: 'entity.created_at',
          correlation_field: 'account_id',
          // timetravel
          map: [
            {
              from: 'entity.account_id',
              to: 'account_id',
            },
          ],
        },
      ]);

      expect(data).toMatchObject({
        entity: {
          version: 0,
          firstname: `Alice ${uuid}`,
        },
        account: {
          firstname: `Alice ${uuid}`,
          version: 0,
        },
      });
    });
  });
});

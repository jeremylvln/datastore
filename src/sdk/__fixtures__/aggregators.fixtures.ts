import type { HandlerBuilderFunc, HandlerConfig } from '../../typings';

import Datastore from '../Datastore';
import Aggregator from '../aggregator/Aggregator';

export const main: HandlerBuilderFunc = async function (
  url: URL,
): Promise<HandlerConfig> {
  const datastore = new Datastore({
    baseUrl: process.env.DATASTORE_API_URL || 'http://localhost:3001',
    token: process.env.DATASTORE_ACCESS_TOKEN || 'token',
    debug: false,
  });

  const datastores = new Map([['models', datastore]]);

  const aggregator = new Aggregator(datastores);

  let count = 0;

  return {
    datastore: url.searchParams.get('datastore') || 'models',
    model: url.searchParams.get('model') || 'configs',
    source: url.searchParams.get('source') || 'entities',
    raw: false,
    query: {},
    start: async () => ({
      datastores: {
        models: datastore,
      },
    }),
    stop: async () => {
      console.info('Count', count);
    },
    handler: async (obj: any) => {
      count += 1;

      const projected = await aggregator.aggregate(
        [
          {
            type: 'fetch',
            datastore: 'models',
            model: 'configs',
            source: 'events',
            destination: 'events',
            map: [
              {
                from: 'entity.config_id',
                to: 'config_id',
                default: null,
              },
            ],
          },
          {
            type: 'map',
            map: [
              {
                from: 'entity.created_at',
                to: 'created_at',
                default: null,
              },
            ],
          },
          {
            type: 'unset',
            path: 'entity',
          },
        ],
        { entity: obj },
      );

      console.info('Projected:', projected);
    },
  };
};

import type { HandlerBuilderFunc, HandlerConfig } from '../../typings';

import Datastore from '../Datastore';

export const log: HandlerBuilderFunc = async function (
  url: URL,
): Promise<HandlerConfig> {
  const datastore = new Datastore({
    baseUrl: process.env.DATASTORE_API_URL || 'http://localhost:3001',
    token: process.env.DATASTORE_ACCESS_TOKEN || 'token',
    debug: false,
  });

  let count = 0;

  return {
    datastore: url.searchParams.get('datastore') || 'models',
    model: url.searchParams.get('model') || 'all',
    source: url.searchParams.get('source') || 'events',
    raw: false,
    query: {},
    start: async () => ({
      datastores: {
        models: datastore,
      },
    }),
    stop: async () => {
      console.info('Count', count);
    },
    handler: async (obj: any) => {
      count += 1;
      console.info('Object received:', { obj });
    },
  };
};

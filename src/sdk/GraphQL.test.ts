import Core from './Core';
import GraphQL from './GraphQL';

describe('sdk/GraphQL', () => {
  let core;
  let client;
  beforeEach(() => {
    core = new Core();
    client = new GraphQL({}, core);
    client.core._axios = {
      request: jest.fn(),
    };
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('constructor', () => {
    it('creates a new client with default configuration', () => {
      client = new GraphQL({}, core);

      expect(client._config).toEqual({});
    });
  });

  /**
   * GraphQL
   */
  describe('#graphql', () => {
    beforeEach(() => {
      client.core._axios = {
        request: jest.fn().mockImplementation(() => ({
          data: {},
        })),
      };
    });

    afterEach(() => {
      jest.restoreAllMocks();
    });

    it('calls the graphql model route with query parameter', async () => {
      client.core._axios = {
        request: jest.fn().mockImplementation(() => ({
          data: {
            data: {
              viewerApiKey: {
                user: {
                  firstname: 'John',
                },
              },
            },
          },
        })),
      };

      const { data } = await client.request('query', `user`);

      expect(client.core._axios.request).toHaveBeenLastCalledWith({
        method: 'post',
        url: '/api/graphql',
        data: {
          query: `
query Op($token: String!) {
  viewerApiKey(apiKey: $token) {
    user
  }
}`,
          operationName: 'Op',
          variables: {
            token: 'token',
          },
        },
      });

      expect(data).toEqual({
        data: {
          user: {
            firstname: 'John',
          },
        },
      });
    });

    it('calls the graphql model route with specified variables', async () => {
      await client.request('query', `user`, {
        name: ['String', 'John'],
      });

      expect(client.core._axios.request).toHaveBeenLastCalledWith({
        method: 'post',
        url: '/api/graphql',
        data: {
          query: `
query Op($token: String!, $name: String) {
  viewerApiKey(apiKey: $token) {
    user
  }
}`,
          operationName: 'Op',
          variables: {
            name: 'John',
            token: 'token',
          },
        },
      });
    });

    it('calls the graphql model route with an operationName', async () => {
      await client.request('query', `user`, {}, 'Operation');

      expect(client.core._axios.request).toHaveBeenLastCalledWith({
        method: 'post',
        url: '/api/graphql',
        data: {
          query: `
query Operation($token: String!) {
  viewerApiKey(apiKey: $token) {
    user
  }
}`,
          operationName: 'Operation',
          variables: {
            token: 'token',
          },
        },
      });
    });

    it('queries graphql', async () => {
      await client.query(`user`, {}, 'Operation');

      expect(client.core._axios.request).toHaveBeenLastCalledWith({
        method: 'post',
        url: '/api/graphql',
        data: {
          query: `
query Operation($token: String!) {
  viewerApiKey(apiKey: $token) {
    user
  }
}`,
          operationName: 'Operation',
          variables: {
            token: 'token',
          },
        },
      });
    });

    it('requests a mutation on graphql', async () => {
      await client.mutation(`updateUser{}`);

      expect(client.core._axios.request).toHaveBeenLastCalledWith({
        method: 'post',
        url: '/api/graphql',
        data: {
          query: `
mutation Op($token: String!) {
  mutationViewerApiKey(apiKey: $token) {
    updateUser{}
  }
}`,
          operationName: 'Op',
          variables: {
            token: 'token',
          },
        },
      });
    });

    it('calls the graphql model route for mutation', async () => {
      await client.request(
        'mutation',
        `updateUser(userId: "user_id") { firstname: "Alice" }`,
        {
          name: ['String', 'John'],
        },
      );

      expect(client.core._axios.request).toHaveBeenLastCalledWith({
        method: 'post',
        url: '/api/graphql',
        data: {
          query: `
mutation Op($token: String!, $name: String) {
  mutationViewerApiKey(apiKey: $token) {
    updateUser(userId: "user_id") { firstname: "Alice" }
  }
}`,
          operationName: 'Op',
          variables: {
            name: 'John',
            token: 'token',
          },
        },
      });
    });

    it('returns errors', async () => {
      client.core._axios = {
        request: jest.fn().mockImplementation(() => ({
          data: {
            errors: ['Ooops'],
            data: {
              mutationViewerApiKey: {
                user: {
                  firstname: 'John',
                },
              },
            },
          },
        })),
      };

      const { data } = await client.request(
        'mutation',
        `updateUser(userId: "user_id") { firstname: "Alice" }`,
        {
          name: ['String', 'John'],
        },
      );

      expect(data).toEqual({
        errors: ['Ooops'],
        data: {
          user: {
            firstname: 'John',
          },
        },
      });
    });
  });
});

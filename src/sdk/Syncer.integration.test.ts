import type Datastore from './Datastore';

import setup from '../../test/setup';

import Syncer from './Syncer';

import PostgreSQLClient from '../services/pg';

import thingsModelConfig from '../templates/examples/things.json';

describe('sdk/Syncer (integration)', () => {
  let app;
  let ds;
  let datastores;
  let pg;
  let client: Syncer;

  beforeAll(async () => {
    [, , , , , ds as Datastore, , app] = await setup.startApi({
      features: {
        api: {
          admin: true,
          updateSpecOnModelsChange: true,
        },
      },
    });

    datastores = new Map([['default', ds]]);

    await PostgreSQLClient.init(app.services.config.pg);

    pg = new PostgreSQLClient(app.services.config.pg);

    await pg.connect();

    client = new Syncer({}, datastores, pg);
  });

  afterAll(async () => {
    jest.restoreAllMocks();

    await pg.disconnect();

    await setup.teardownDb(app.services.mongodb);

    await setup.stopApi(app);
  });

  describe('#syncData', () => {
    let uuid;

    beforeEach(() => {
      uuid = setup.uuid();
    });

    it('sync entities from a datastore to postgres', async () => {
      const modelName = `users_${uuid}`;
      const modelConfig = {
        ...thingsModelConfig,
        name: modelName,
        correlation_field: 'user_id',
        description: 'Users information',
      };

      await ds.createModel(modelConfig);

      await pg.queryAll(
        PostgreSQLClient.getSqlSchemaForModels([modelConfig], true),
      );

      const { data: entity } = await ds.create(modelName, {
        firstname: 'John',
      });

      await client.syncDataSource('default', [modelConfig], 'entities');

      const result = await pg.query(`SELECT * FROM ${modelName}`);

      expect(result.rows).toMatchObject([
        {
          entity,
        },
      ]);
    });
  });
});

import Datastore from './Datastore';
import Metabase from './Metabase';
import Model from './Model';

import Aggregator from './aggregator/Aggregator';
import * as projections from './projections';

function fnToHandler(fn: any): string {
  const handler = fn.toString();

  return handler.split('\n').slice(1, -1).join('\n');
}

export { Aggregator, Datastore, Metabase, Model, projections, fnToHandler };

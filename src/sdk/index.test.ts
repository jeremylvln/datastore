import { fnToHandler } from './index';

describe('sdk', () => {
  describe('#fnToHandler', () => {
    it('returns stringified version of a function', () => {
      expect(
        fnToHandler(function () {
          const a = 1;
        }).trim(),
      ).toEqual('const a = 1;');
    });
  });
});

{
  "name": "@getanthill/datastore",
  "description": "Event-Sourced Datastore",
  "version": "0.86.1",
  "main": "dist/sdk/index.js",
  "bin": {
    "datastore": "scripts/cli.js",
    "ds": "scripts/cli.js"
  },
  "engines": {
    "node": ">=16"
  },
  "keywords": [
    "anthill",
    "event-source",
    "data",
    "open-api"
  ],
  "author": "Gilles Rasigade",
  "license": "MIT",
  "scripts": {
    "build": "npm run build:clean && npm run build:api && npm run build:sdk",
    "build:clean": "rm -rf dist",
    "build:api": "tsc --build tsconfig.json",
    "build:sdk": "tsc --build tsconfig.sdk.json",
    "build:tsc:clean": "find ./src \\( -iname \\*.js -o -iname \\*.js.map -o -iname \\*.d.ts \\) -type f -delete",
    "dev": "ADMIN_ACCESS_TOKENS=token nodemon --delay 1500ms --signal SIGTERM --watch \"src/**\" --ext \"ts,json\" --ignore \"src/**/*.test.ts\" --exec \"ts-node src/server.ts\"",
    "dev:prettier:write": "prettier --write --ignore-unknown 'scripts/**/*' 'src/**/*'",
    "prepare": "husky install",
    "start": "node --no-warnings dist/server",
    "test": "npm run test:prettier && npm run test:coverage --silent ./src ./test/integration",
    "test:band": "npm run test:prettier && npm run test:coverage --silent --runInBand ./src ./test/integration",
    "test:ci:gitlab": "./scripts/gitlab-test.sh",
    "test:bench:jest": "node --inspect=9222 --expose-gc ./node_modules/.bin/jest --runInBand ./test/bench",
    "test:jest": "node --no-warnings ./node_modules/.bin/jest --runInBand",
    "test:bench": "node --no-warnings ./scripts/bench.js",
    "test:bench:__": "node --inspect=9222 --expose-gc --trace-deopt ./scripts/bench.js",
    "test:coverage": "node --no-warnings ./node_modules/.bin/jest --coverage",
    "test:watch": "jest --watchAll --coverage --coverage-reporters=lcov",
    "test:watch:no-coverage": "jest --watchAll",
    "test:bench:watch": "node ./node_modules/.bin/jest --config=jest.bench.config.js --runInBand --watchAll",
    "test:bench:time:watch": "npm run test:bench:watch ./test/bench/time",
    "test:bench:memory:watch": "npm run test:bench:watch ./test/bench/memory",
    "test:prettier": "prettier -l --ignore-unknown 'scripts/**/*' -l 'src/**/*'"
  },
  "peerDependencies": {
    "@getanthill/telemetry": "1.11.x",
    "@shelf/jest-mongodb": "4.x"
  },
  "dependencies": {
    "@getanthill/api-validators": "1.3.0",
    "@getanthill/event-source": "0.15.0",
    "@getanthill/mongodb-connector": "^1.4.0",
    "ajv": "8.13.0",
    "ajv-formats": "3.0.1",
    "ajv-keywords": "5.1.0",
    "amqplib": "0.10.4",
    "async-mqtt": "2.6.3",
    "axios": "^1.7.2",
    "body-parser": "^1.20.2",
    "commander": "11.1.0",
    "compression": "^1.7.4",
    "cookie-parser": "1.4.6",
    "eventsource": "2.0.2",
    "express": "^4.19.2",
    "express-graphql": "^0.12.0",
    "fast-json-patch": "3.1.1",
    "graphql": "16",
    "helmet": "7.1.0",
    "http-status-codes": "^2.3.0",
    "js-yaml": "4.1.0",
    "lodash": "4.17.21",
    "mongodb": "^6",
    "openapi-to-graphql": "3.0.7",
    "pg": "8.11.5",
    "qs": "6.12.1",
    "shell-quote": "1.8.1"
  },
  "devDependencies": {
    "@airbnb/node-memwatch": "2.0.0",
    "@babel/core": "^7.24.5",
    "@babel/plugin-syntax-dynamic-import": "^7.8.3",
    "@babel/plugin-transform-runtime": "^7.24.3",
    "@babel/preset-env": "^7.24.5",
    "@babel/preset-typescript": "^7.24.1",
    "@babel/runtime": "^7.24.5",
    "@commitlint/cli": "19.3.0",
    "@commitlint/config-conventional": "19.2.2",
    "@semantic-release/changelog": "6.0.3",
    "@semantic-release/gitlab": "13.1.0",
    "@shelf/jest-mongodb": "4.3.2",
    "@types/amqplib": "0.10.5",
    "@types/compression": "^1.7.5",
    "@types/cookie-parser": "1.4.7",
    "@types/eventsource": "1.1.15",
    "@types/express": "4.17.21",
    "@types/jest": "^29.5.12",
    "@types/js-yaml": "4.0.9",
    "@types/json-schema": "7.0.15",
    "@types/lodash": "4.17.4",
    "@types/node": "^20.12.12",
    "@types/shell-quote": "1.7.5",
    "@types/uuid": "9.0.8",
    "@types/ws": "8.5.10",
    "@typescript-eslint/eslint-plugin": "7.10.0",
    "@typescript-eslint/parser": "7.10.0",
    "babel-jest": "^29.7.0",
    "eslint": "9.3.0",
    "eslint-config-prettier": "9.1.0",
    "husky": "9.0.11",
    "jest": "^29.7.0",
    "json-schema": "0.4.0",
    "nodemon": "3.1.0",
    "openapi-types": "12.1.3",
    "prettier": "^3.2.5",
    "semantic-release": "23.1.1",
    "supertest": "7.0.0",
    "ts-node": "10.9.2",
    "tslib": "^2.6.2",
    "typescript": "5.4.5",
    "uuid": "9.0.1",
    "validate-branch-name": "1.3.0"
  },
  "repository": {
    "type": "git",
    "url": "git+ssh://git@gitlab.com/getanthill/datastore.git"
  },
  "bugs": {
    "url": "https://gitlab.com/getanthill/datastore/issues"
  },
  "homepage": "https://gitlab.com/getanthill/datastore#readme",
  "files": [
    "scripts",
    "dist",
    "src"
  ],
  "release": {
    "branches": [
      "master"
    ],
    "plugins": [
      "@semantic-release/commit-analyzer",
      "@semantic-release/release-notes-generator",
      [
        "@semantic-release/changelog",
        {
          "changelogFile": "CHANGELOG.md"
        }
      ],
      [
        "@semantic-release/npm",
        {
          "npmPublish": true
        }
      ],
      "@semantic-release/git",
      [
        "@semantic-release/gitlab",
        {
          "assets": [
            {
              "path": "CHANGELOG.md"
            },
            {
              "path": "package.json"
            }
          ]
        }
      ]
    ],
    "preset": "angular"
  }
}

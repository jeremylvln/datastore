module.exports = {
  // @see https://stackoverflow.com/a/44366115
  testEnvironment: 'node',
  testTimeout: 1000000,
  globalSetup: '<rootDir>/test/setup.ts',
  globalTeardown: '<rootDir>/test/teardown.ts',
  transform: {
    '^.+\\.(ts|js)$': 'babel-jest',
  },
};

module.exports = {
  mongodbMemoryServerOptions: {
    binary: {
      version: '4.4.15',
      skipMD5: true,
    },
    autoStart: true,
    instance: {
      dbName: 'datastore',
    },
    replSet: {
      count: 3,
      storageEngine: 'wiredTiger',
    },
  },
};

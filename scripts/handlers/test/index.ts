import type {
  HandlerConfig,
  RunnerServices,
  Source,
} from '@getanthill/datastore/dist/typings';

function wait(delayInMilliseconds: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, delayInMilliseconds));
}

function blockEventLoop(delayInMilliseconds: number): void {
  const tic = Date.now();
  let blockedSince = 0;
  while (blockedSince < delayInMilliseconds) {
    blockedSince = Date.now() - tic;
  }
}

export async function log(url: URL, services): Promise<HandlerConfig> {
  let count = 0;

  const datastore = url.searchParams.get('datastore') ?? 'models';
  const model = url.searchParams.get('model') ?? 'communications';
  const source = (url.searchParams.get('source') ?? 'events') as Source;
  const query = JSON.parse(url.searchParams.get('query') ?? '{}');
  const timeout = parseInt(url.searchParams.get('timeout') ?? '10', 10);
  const block = parseInt(url.searchParams.get('block') ?? '0', 10);
  const withHeartbeat = url.searchParams.get('with_heartbeat') === 'true';
  const withFetch = url.searchParams.get('with_fetch') === 'true';

  return {
    datastore,
    model,
    source,
    raw: false,
    query,
    start: async () => {
      services.telemetry.logger.info('[tests#log] Starting');

      return services as unknown as RunnerServices;
    },
    stop: async () => {
      services.telemetry.logger.info('[tests#log] Ending');
    },
    handler: async (event: Metric) => {
      count += 1;
      services.telemetry.logger.info('[tests#log] Handling', {
        count,
      });

      services.telemetry.logger.debug('[tests#log] Waiting', {
        count,
      });
      await wait(timeout);

      if (withHeartbeat === true) {
        services.telemetry.logger.debug('[tests#log] Heartbeat', {
          count,
        });
        await services.datastores.models.heartbeat();
      }

      if (withFetch === true) {
        services.telemetry.logger.debug('[tests#log] Fetch', {
          count,
        });
        await services.datastores.models.find(model, {}, 0, 1000);
      }

      services.telemetry.logger.debug('[tests#log] Block', {
        count,
      });
      blockEventLoop(block);

      count -= 1;
      services.telemetry.logger.info('[tests#log] Handled', {
        count,
      });
    },
  };
}

export async function exception(
  url: URL,
  services = build(),
): Promise<HandlerConfig> {
  return {
    datastore: 'models',
    model: 'metrics',
    source: 'entities',
    raw: false,
    query: {},
    start: async () => {
      services.telemetry.logger.info('[tests#exception] Starting');

      return services as unknown as RunnerServices;
    },
    stop: async () => {
      services.telemetry.logger.info('[tests#exception] Ending');
    },
    handler: async () => {
      services.telemetry.logger.info('[tests#exception] Handling', {});

      throw new Error('Ooops');
    },
  };
}

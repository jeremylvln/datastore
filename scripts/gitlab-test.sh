#!/usr/bin/env bash

docker rm gitlab-runner-datastore --force

docker run -d --name gitlab-runner-datastore --restart always \
  -v $PWD:$PWD \
  -v ${HOME}/development/lab/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest

docker exec -it gitlab-runner-datastore bash -c "\
  cd $PWD; \
  git config --global --add safe.directory $PWD; \
  gitlab-runner exec docker test\
"
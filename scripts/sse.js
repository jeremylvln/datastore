const EventSource = require('eventsource');

const evtSource = new EventSource(
  'http://localhost:3001/api/stream/all/events/sse',
  {
    headers: {
      authorization: 'token',
    },
  },
);

evtSource.onmessage = function (event, data = JSON.parse(event.data)) {
  console.log('onMessage', event, data);
};

#!/bin/env node

const { default: PostgreSQLClient } = require('../dist/services/pg');
const { default: config } = require('../dist/config');

async function main() {
  console.log('[wait] Waiting for Postgres connection...');
  let connected = false;
  const pg = new PostgreSQLClient(config.pg);
  do {
    try {
      await pg.connect();
      connected = true;
    } catch (err) {
      connected = false;
    } finally {
      await pg.disconnect();
    }
  } while (connected === false);
  console.log('[wait] Postgres connection is up');
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});

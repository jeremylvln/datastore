#!/bin/env node

const services = require('../dist/services').default;

async function main() {
  console.log('[wait] Waiting for RabbitMQ connection...');
  let connected = false;
  do {
    try {
      await services.amqp.connect();

      connected = true;
    } catch (err) {
      connected = false;
    } finally {
      await services.amqp.end();
    }
  } while (connected === false);

  console.log('[wait] RabbitMQ connection is up');
}

main().catch((err) => {
  console.error(err);

  process.exit(1);
});

#!/usr/bin/env bash
#
# @credits https://medium.com/@pkostohrys/run-mongodb-replica-set-using-docker-1d8f45a03a29

docker stop m1 m2 m3 m4 m5 arbiter
docker rm m1 m2 m3 m4 m5 arbiter

docker network create mongo-cluster

docker run -d --name m1 --net mongo-cluster mongo:4.4 mongod --replSet rs

sleep 5

docker exec m1 mongo --eval 'rs.initiate({"_id":"rs","members":[{"_id":0,"host":"m1:27017","priority":1}]})'

docker run -d --name m2 --net mongo-cluster mongo:4.4 mongod --replSet rs
docker run -d --name m3 --net mongo-cluster mongo:4.4 mongod --replSet rs
docker run -d --name m4 --net mongo-cluster mongo:4.4 mongod --replSet rs
docker run -d --name m5 --net mongo-cluster mongo:4.4 mongod --replSet rs

docker run -d --name arbiter --net mongo-cluster mongo:4.4 mongod --replSet rs

sleep 5

docker exec m1 mongo --eval 'rs.reconfig({"_id":"rs","members":[{"_id":0,"host":"m1:27017","priority":1}, {"_id":1,"host":"m2:27017","priority":2}]})'
docker exec m1 mongo --eval 'rs.reconfig({"_id":"rs","members":[{"_id":0,"host":"m1:27017","priority":1}, {"_id":1,"host":"m2:27017","priority":2}, {"_id":2,"host":"m3:27017","priority":3}]})'
docker exec m1 mongo --eval 'rs.reconfig({"_id":"rs","members":[{"_id":0,"host":"m1:27017","priority":1}, {"_id":1,"host":"m2:27017","priority":2}, {"_id":2,"host":"m3:27017","priority":3}, {"_id":3,"host":"m4:27017","priority":4}]})'
docker exec m1 mongo --eval 'rs.reconfig({"_id":"rs","members":[{"_id":0,"host":"m1:27017","priority":1}, {"_id":1,"host":"m2:27017","priority":2}, {"_id":2,"host":"m3:27017","priority":3}, {"_id":3,"host":"m4:27017","priority":4}, {"_id":4,"host":"m5:27017","priority":5}]})'

# docker exec m1 mongo --eval 'rs.add("arbiter:27017")'
docker exec m1 mongo --eval 'rs.addArb("arbiter:27017")'

exit

docker build --target test --build-arg NPM_TOKEN=${NPM_TOKEN} . --tag datastore:test

docker stop datastore
docker rm datastore

docker run -it --name datastore --network mongo-cluster \
  --mount type=bind,source="$(pwd)",target=/app \
  -p 3001:3001 \
  -e ADMIN_ACCESS_TOKENS="token" \
  -e NODE_ENV="development" \
  -e FEATURE_CORS_ENABLED="false" \
  -e FEATURE_API_TEMPLATES="true" \
  -e FEATURE_API_AGGREGATE="true" \
  -e FEATURE_API_ADMIN="true" \
  -e TELEMETRY_LOGGER_JSON="false" \
  -e TELEMETRY_LOGGER_LEVEL="50" \
  -e TELEMETRY_METER_EXPORTER="noop" \
  -e TELEMETRY_TRACER_EXPORTER="noop" \
  -e SECURITY_ENCRYPTION_KEYS='{"all":["03d72e2f7d7286beacee33a3326ae06a"]}' \
  -e MONGO_WRITE_URL='mongodb://m1:27017,m2:27017,m3:27017,m4:27017,m5:27017/datastore?readPreference=primary&w=1' \
  -e MONGO_READ_URL='mongodb://m1:27017,m2:27017,m3:27017,m4:27017,m5:27017/datastore?readPreference=secondaryPreferred' \
  datastore bash -c "npm run dev"

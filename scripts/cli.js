#!/usr/bin/env node

process.removeAllListeners('warning');

process.env.OTEL_PROMETHEUS_EXPORTER_PREVENT_SERVER_START =
  process.env.OTEL_PROMETHEUS_EXPORTER_PREVENT_SERVER_START || 'true';

const { cli } = require('../dist/cli');

cli();

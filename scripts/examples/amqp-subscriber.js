const amqplib = require('amqplib');
const { telemetry } = require('@getanthill/telemetry');

const { default: config } = require('../../dist/config');

const { default: AmqpClient } = require('../../dist/services/amqp');

let connection;
let channel;
async function connect() {
  try {
    console.log('[amqp] Connecting...');
    connection = await amqplib.connect(config.url, config.options);
    console.log('[amqp] Connected ✅');

    connection.on('close', () => {
      console.log('[amqp] Connection closed...');

      setTimeout(connect, 1000);
    });

    return init();
  } catch (err) {
    console.error(err);

    setTimeout(connect, 1000);
  }
}

async function init() {
  console.log('[amqp] Creating channel...');
  channel = await connection.createChannel();
  console.log('[amqp] Channel created ✅');
}

async function main() {
  console.log('[amqp] Testing');

  const client = new AmqpClient(config.amqp, telemetry);

  await client.connect();
  await client.init();

  await client.subscribe('topic', {});
}

main().catch(console.error);

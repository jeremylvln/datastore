#!/usr/bin/env bash
#
# @see https://vrelevant.net/openssl-self-signed-certs-2023/
# @see https://www.matteomattei.com/client-and-server-ssl-mutual-authentication-with-nodejs/

rm -rf ./certs/*

echo -e "[ca] Certificate Authority generation"
openssl req -x509 -noenc -newkey rsa:4096 \
  -days 36500 \
  -subj '/CN=getanthill.org' \
  -keyout ./certs/ca_key.pem \
  -out ./certs/ca_cert.pem \

echo -e "[server] Certificate on 'server.aaa.com'"
echo -e "[server] Certificate Signing Request (CSR)"
echo -e "> Create Service Private Key and Certificate Signing Request"
openssl req -noenc -newkey rsa:4096 \
  -subj '/CN=server.aaa.com' \
  -keyout ./certs/server_key.pem \
  -out ./certs/server_cert.csr

echo -e "[server] Create public key self-signed by CA"
echo -e "> Create service cert/public key, signed by self-signed CA"
openssl x509 -req -days 36500 \
  -in ./certs/server_cert.csr \
  -CA ./certs/ca_cert.pem \
  -CAkey ./certs/ca_key.pem \
  -CAcreateserial \
  -out ./certs/server_cert.pem

echo -e "Client certificate"
echo -e "[client] Certificate Signing Request (CSR)"
openssl req -noenc -newkey rsa:4096 \
  -subj '/CN=whatever' \
  -keyout ./certs/client_key.pem \
  -out ./certs/client_cert.csr

echo -e "[client] Create public key self-signed by CA"
openssl x509 -req -days 36500 \
  -in ./certs/client_cert.csr \
  -CA ./certs/ca_cert.pem \
  -CAkey ./certs/ca_key.pem \
  -CAcreateserial \
  -out ./certs/client_cert.pem

echo -e "[host] Require to register server.aaa.com in your hosts file"
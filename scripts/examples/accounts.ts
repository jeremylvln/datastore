import type { ModelConfig } from '../../dist';
import { Datastore, constants as c } from '../../dist';

const MODEL_DATABASE: string = 'datastore';
const MODEL_NAME: string = 'accounts';
const CORRELATION_FIELD: string = 'account_id';

const firstname = {
  ...c.COMPONENT_STRING,
  description: 'Firstname of the user',
  example: 'John',
};

const properties = {
  firstname,
};

const modelConfig: ModelConfig = {
  is_enabled: false,
  db: MODEL_DATABASE,
  name: MODEL_NAME,
  correlation_field: CORRELATION_FIELD,
  indexes: [],
  schema: {
    model: {
      type: 'object',
      additionalProperties: false,
      required: ['version'],
      properties: {
        [CORRELATION_FIELD]: c.COMPONENT_CORRELATION_ID,
        ...properties,
      },
    },
    events: {
      [c.EVENT_TYPE_CREATED]: {
        '0_0_0': {
          required: ['firstname'],
          properties,
        },
      },
      [c.EVENT_TYPE_UPDATED]: {
        '0_0_0': {
          properties,
        },
      },
      [c.EVENT_TYPE_RESTORED]: {
        '0_0_0': {
          properties,
        },
      },
    },
  },
};

async function main() {
  const datastore = new Datastore({
    baseUrl: 'http://localhost:3001',
    token: 'token',
    debug: true,
  });

  try {
    await datastore.createModel(modelConfig);
  } catch (err) {
    await datastore.updateModel(modelConfig);
  }

  await datastore.createModelIndexes(modelConfig);
}

main()
  .then(() => {
    console.log('Model created');
    console.log('Restart the Datastore to use this newly created model:');
    console.log('> docker restart datastore');
    console.log('then http://localhost:3001/doc#tag/Accounts');
  })
  .catch((err) => {
    console.error(err);

    process.exit(1);
  });

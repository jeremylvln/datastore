#!/bin/env node

const services = require('../dist/services').default;

async function main() {
  console.log('[wait] Waiting for MongoDb connection...');
  let connected = false;
  do {
    try {
      await services.mongodb.connect();
      connected = true;
    } catch (err) {
      connected = false;
    }
  } while (connected === false);

  console.log('[wait] MongoDb connection is up');
  await services.mongodb.disconnect();
}

main().catch((err) => {
  console.error(err);

  process.exit(1);
});

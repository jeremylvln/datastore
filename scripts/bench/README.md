# Benchmark

## Results

```shell
drill --stats --quiet --nanosec --benchmark drill-probe-request.yaml
```

|     |            Scenario |  `req/s` | `median (ms)` | `average (ms)` | `std deviation (ms))` |
| --: | ------------------: | -------: | ------------: | -------------: | --------------------: |
| `0` | Node.js HTTP Server | `15 537` |       `4.811` |        `5.294` |               `2.952` |
| `1` |   + body JSON parse | `13 719` |       `5.808` |        `6.273` |               `2.623` |
| `2` |    + MongoDB insert |  `2 133` |      `42.086` |       `45.891` |              `12.334` |
| `3` |           + Fastify |  `2 105` |      `42.556` |       `46.509` |              `10.259` |
| `4` |           ~ Express |  `1 344` |      `69.771` |       `73.487` |              `14.980` |
| `5` |         ~ Datastore |    `435` |     `226.427` |      `228.066` |              `28.134` |

<details>
  <summary>Configuration</summary>

```shell
lscpu

Architecture:            x86_64
  CPU op-mode(s):        32-bit, 64-bit
  Address sizes:         46 bits physical, 48 bits virtual
  Byte Order:            Little Endian
CPU(s):                  8
  On-line CPU(s) list:   0-7
Vendor ID:               GenuineIntel
  Model name:            Intel(R) Xeon(R) CPU @ 2.30GHz
    CPU family:          6
    Model:               63
    Thread(s) per core:  2
    Core(s) per socket:  4
    Socket(s):           1
    Stepping:            0
    BogoMIPS:            4599.99
    Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse
                         36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm const
                         ant_tsc rep_good nopl xtopology nonstop_tsc cpuid tsc_known_freq pni
                         pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt aes x
                         save avx f16c rdrand hypervisor lahf_lm abm invpcid_single pti ssbd i
                         brs ibpb stibp fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid x
                         saveopt arat md_clear arch_capabilities
Virtualization features:
  Hypervisor vendor:     KVM
  Virtualization type:   full
Caches (sum of all):
  L1d:                   128 KiB (4 instances)
  L1i:                   128 KiB (4 instances)
  L2:                    1 MiB (4 instances)
  L3:                    45 MiB (1 instance)
NUMA:
  NUMA node(s):          1
  NUMA node0 CPU(s):     0-7
Vulnerabilities:
  Itlb multihit:         Not affected
  L1tf:                  Mitigation; PTE Inversion
  Mds:                   Mitigation; Clear CPU buffers; SMT Host state unknown
  Meltdown:              Mitigation; PTI
  Spec store bypass:     Mitigation; Speculative Store Bypass disabled via prctl and seccomp
  Spectre v1:            Mitigation; usercopy/swapgs barriers and __user pointer sanitization
  Spectre v2:            Mitigation; Retpolines, IBPB conditional, IBRS_FW, STIBP conditional,
                          RSB filling
  Srbds:                 Not affected
  Tsx async abort:       Not affected
```

</details>

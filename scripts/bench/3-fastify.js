const fastify = require('fastify');
const { MongoClient } = require('mongodb');

async function main() {
  const app = fastify();
  const client = new MongoClient(
    process.env.MONGO_URL || 'mongodb://localhost:27017/test',
  );

  await client.connect();

  app.get('/heartbeat', async (req, res) => {
    res.send({ status: 'up' });
  });

  app.post('/api/things', async (req, res) => {
    const post = req.body;

    await client.db().collection('test').insertOne(post);

    res.send(post);
  });

  app.listen({ port: 3001 });
  console.log('listening on port 3001');
}

main().catch(console.error);

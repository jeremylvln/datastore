#!/usr/bin/env bash
set -e

PORT=3001 \
MONGO_URL='mongodb://localhost:27017/datastore?replicaSet=rs' \
NODE_ENV=development \
FEATURE_API_TEMPLATES=true \
FEATURE_API_ADMIN=true \
TOKENS='[{ "id": "admin", "token": "token", "level": "admin" }]' \
TELEMETRY_LOGGER_LEVEL='debug' \
TELEMETRY_METER_EXPORTER=noop \
TELEMETRY_TRACER_EXPORTER=noop \
FEATURE_AMQP_IS_ENABLED=true \
AMQP_NAMESPACE=datastore \
AMQP_URL='["amqp://guest:guest@localhost:5672", "amqp://guest:guest@localhost:5673"]' \
AMQP_QUEUE_CONSUMER_NAME=datastore \
SECURITY_ENCRYPTION_KEYS='{"all":["03d72e2f7d7286beacee33a3326ae06a"]}' \
  node dist/server.js
import jestMongodb from '@shelf/jest-mongodb/jest-preset';

export default async function teardown() {
  console.log('Global Teardown');

  const { default: teardownMongoDb } = await import(jestMongodb.globalTeardown);

  process.env.SKIP_MONGODB_IN_MEMORY !== 'true' &&
    (await teardownMongoDb({
      rootDir: '.',
    }));
}

import request from 'supertest';
import express from 'express';

import api from '../../../src/api';

import setup from '../../setup';

describe('/api/aggregate', () => {
  let mongodb;
  let models;
  let app;
  let services;

  beforeEach(async () => {
    app = await setup.build({
      mode: 'development',
      security: {
        tokens: [
          {
            id: 'read',
            level: 'read',
            token: 'token',
          },
        ],
      },
      features: {
        api: {
          admin: true,
          aggregate: true,
        },
      },
    });

    services = app.services;
    mongodb = services.mongodb;

    models = await setup.initModels(services);

    app = express();

    app
      .get('/heartbeat', (_req, res): void => {
        res.json({ is_alive: true });
      })
      .use('/api', await api({ ...services, models }));
  });

  afterEach(async () => {
    await setup.teardownDb(mongodb);
  });

  it('returns the aggregate', async () => {
    const res = await request(app)
      .post('/api/aggregate')
      .set('Accept', 'application/json')
      .set(
        'Authorization',
        services.config.security.tokens.filter(
          (access) => access.level === 'read',
        )[0],
      )
      .send([]);

    expect(res.body).toMatchObject({});
  });
});

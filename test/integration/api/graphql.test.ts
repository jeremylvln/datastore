import request from 'supertest';
import express from 'express';

import api from '../../../src/api';

import setup from '../../setup';

describe('/api - nomi', () => {
  let mongodb;
  let models;
  let app;
  let services;

  beforeEach(async () => {
    app = await setup.build({
      mode: 'development',
      security: {
        tokens: [
          {
            id: 'read',
            level: 'read',
            token: 'token',
          },
        ],
      },
      features: {
        api: {
          graphql: true,
        },
      },
    });

    services = app.services;
    mongodb = services.mongodb;

    models = await setup.initModels(services);

    app = express();

    app
      .get('/heartbeat', (_req, res): void => {
        res.json({ is_alive: true });
      })
      .use('/api', await api({ ...services, models }));
  });

  afterEach(async () => {
    await setup.teardownDb(mongodb);
  });

  it.skip('answers on the route /api/graphql', async () => {
    const res = await request(app).get('/api/graphql');

    expect(res.statusCode).toEqual(200);
  });
});

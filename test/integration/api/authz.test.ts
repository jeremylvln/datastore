import * as authz from '../../../src/typings/authorizations';

import request from 'supertest';

import setup from '../../setup';

describe('/api - authz', () => {
  let mongodb;
  let app;
  let instance;
  let sdk;

  beforeAll(async () => {
    [, mongodb, , app, , sdk, , instance] = await setup.startApi({
      authz: {
        isEnabled: true,
        noPolicyVerb: 'allow',
      },
    });
  });

  beforeEach(async () => {
    jest.restoreAllMocks();

    instance.services.config.authz.skipModels = [];
    instance.services.config.authz.onlyModels = [];

    await setup.cleanModel(instance.services, 'attributes');
    await setup.cleanModel(instance.services, 'policies');
  });

  afterAll(async () => {
    await setup.teardownDb(mongodb);

    await setup.stopApi(instance);
  });

  it('answers on the heartbeat', async () => {
    const res = await request(app).get('/heartbeat');

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      is_alive: true,
    });
  });

  it('answers on the ready route', async () => {
    const res = await request(app).get('/ready');

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      is_ready: true,
    });
  });

  it('blocks requests on specific models put in readonly', async () => {
    await instance.services.models.factory('policies').create({
      name: 'model_attributes_readonly',
      description: 'Model attributes is readonly',
      is_enabled: true,
      obligations: [],
      scope: ['object'],
      verb: authz.AUTHORIZATION_VERB_DENY as authz.PolicyVerb,
      rules: [
        {
          name: 'model_attributes_readonly',
          description: 'Model attributes is readonly',
          action: {},
          subject: {},
          object: {
            type: 'object',
            properties: {
              model: {
                type: 'string',
                enum: ['attributes'],
              },
            },
          },
          context: {},
        },
      ],
    });

    const res = await request(app)
      .post('/api/attributes')
      .query({ a: 1 })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    expect(res.statusCode).toEqual(403);
  });

  it('skips requests on specific models configured as skipped models', async () => {
    instance.services.config.authz.skipModels = ['attributes'];
    await instance.services.models.factory('policies').create({
      name: 'model_attributes_readonly',
      description: 'Model attributes is readonly',
      is_enabled: true,
      obligations: [],
      scope: ['object'],
      verb: authz.AUTHORIZATION_VERB_DENY as authz.PolicyVerb,
      rules: [
        {
          name: 'model_attributes_readonly',
          description: 'Model attributes is readonly',
          action: {},
          subject: {},
          object: {
            type: 'object',
            properties: {
              model: {
                type: 'string',
                enum: ['skipped'],
              },
            },
          },
          context: {},
        },
      ],
    });

    const res = await request(app)
      .post('/api/attributes')
      .query({ a: 1 })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    expect(res.statusCode).toEqual(400); // <-- Passed authorization layer
  });

  it('skips requests on specific models configured as only models', async () => {
    instance.services.config.authz.onlyModels = ['attributes'];
    await instance.services.models.factory('policies').create({
      name: 'model_attributes_readonly',
      description: 'Model attributes is readonly',
      is_enabled: true,
      obligations: [],
      scope: ['object'],
      verb: authz.AUTHORIZATION_VERB_DENY as authz.PolicyVerb,
      rules: [
        {
          name: 'model_attributes_readonly',
          description: 'Model attributes is readonly',
          action: {},
          subject: {},
          object: {
            type: 'object',
            properties: {
              model: {
                type: 'string',
                enum: ['attributes'],
              },
            },
          },
          context: {},
        },
      ],
    });

    const res = await request(app)
      .post('/api/unknown')
      .query({ a: 1 })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    expect(res.statusCode).toEqual(400); // <-- Skipped the authorization layer
  });

  it('blocks requests on unknown models', async () => {
    await instance.services.models.factory('policies').create({
      name: 'unknown_models_are_forbidden',
      description: 'Unknown models are forbidden',
      is_enabled: true,
      obligations: [],
      scope: ['action', 'api'],
      verb: authz.AUTHORIZATION_VERB_DENY as authz.PolicyVerb,
      rules: [
        {
          name: 'unknown_models_are_forbidden',
          description: 'Any request is valid',
          action: {},
          subject: {},
          object: {
            type: 'object',
            required: ['model'],
            properties: {
              model: {
                type: 'string',
                enum: ['unknown'],
              },
            },
          },
          context: {},
        },
      ],
    });

    const res = await request(app)
      .post('/api/invalid')
      .query({ a: 1 })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    expect(res.statusCode).toEqual(403);
  });

  it('blocks requests on stream if not authorized by attributes', async () => {
    await instance.services.models.factory('policies').create({
      name: 'stream_authorized',
      description: 'Stream is authorized with the correct attribute',
      is_enabled: true,
      obligations: [],
      scope: ['action', 'api', 'stream'],
      verb: authz.AUTHORIZATION_VERB_DENY as authz.PolicyVerb,
      rules: [
        {
          name: 'stream_attribute_must_be_present',
          description: 'Stream attribute must be present',
          action: {},
          subject: {
            not: {
              type: 'object',
              required: ['_attributes'],
              properties: {
                _attributes: {
                  type: 'array',
                  items: {
                    anyOf: [
                      {
                        type: 'object',
                        required: ['name', 'value'],
                        properties: {
                          name: {
                            type: 'string',
                            enum: ['is_stream_authorized'],
                          },
                          value: {
                            type: 'boolean',
                            enum: [true],
                          },
                        },
                      },
                    ],
                  },
                },
              },
            },
          },
          object: {},
          context: {},
        },
      ],
    });

    const { body } = await request(app)
      .post('/api/attributes')
      .send({
        is_enabled: true,
        scope: ['subject'],
        name: 'is_stream_authorized',
        value: false,
      })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    const res = await request(app).get('/api/stream');

    expect(res.statusCode).toEqual(403);
  });

  it('noops if no obligation is provided', async () => {
    const [, mongodb, , app, , sdk, , instance] = await setup.startApi({
      authz: {
        isEnabled: false,
      },
    });

    const res = await request(app)
      .post('/api/attributes')
      .send({
        scope: ['attributes'],
        name: 'my_attribute',
        value: 12,
      })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    expect({ status: res.statusCode, body: res.body }).toMatchObject({
      status: 400,
      body: {
        status: 400,
        message: 'Invalid Model',
        details: [
          {
            model: 'attributes',
          },
        ],
      },
    });

    await setup.teardownDb(mongodb);

    await setup.stopApi(instance);
  });

  it('returns only fields authorized by the obligations', async () => {
    await instance.services.models.factory('policies').create({
      name: 'model_attributes_readonly',
      description: 'Model attributes is readonly',
      is_enabled: true,
      obligations: [
        {
          name: 'enforce_scope',
          description: 'Enforce the scope of the attribute',
          type: 'pick',
          source: 'body',
          value: ['name', 'value'],
        },
      ],
      scope: ['object'],
      verb: authz.AUTHORIZATION_VERB_ALLOW as authz.PolicyVerb,
      rules: [
        {
          name: 'model_attributes_obligations',
          description: 'Model attributes has obligations',
          action: {},
          subject: {},
          object: {
            type: 'object',
            properties: {
              model: {
                type: 'string',
                enum: ['attributes'],
              },
            },
          },
          context: {},
        },
      ],
    });

    const res = await request(app)
      .post('/api/attributes')
      .send({
        scope: ['attributes'],
        name: 'my_attribute',
        value: 12,
      })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    expect({ status: res.statusCode, body: res.body }).toEqual({
      status: 200,
      body: { name: 'my_attribute', value: 12 },
    });
  });

  it('returns only fields authorized by the obligations in a find query', async () => {
    await instance.services.models.factory('policies').create({
      name: 'model_attributes_readonly',
      description: 'Model attributes is readonly',
      is_enabled: true,
      obligations: [
        {
          name: 'enforce_scope',
          description: 'Enforce the scope of the attribute',
          type: 'pick',
          source: 'body',
          value: ['name', 'value'],
        },
      ],
      scope: ['object'],
      verb: authz.AUTHORIZATION_VERB_ALLOW as authz.PolicyVerb,
      rules: [
        {
          name: 'model_attributes_obligations',
          description: 'Model attributes has obligations',
          action: {},
          subject: {},
          object: {
            type: 'object',
            properties: {
              model: {
                type: 'string',
                enum: ['attributes'],
              },
            },
          },
          context: {},
        },
      ],
    });

    await request(app)
      .post('/api/attributes')
      .send({
        scope: ['attributes'],
        name: 'my_attribute',
        value: 12,
      })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    const res = await request(app)
      .get('/api/attributes')
      .query({
        name: 'my_attribute',
      })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    expect({ status: res.statusCode, body: res.body }).toEqual({
      status: 200,
      body: [{ name: 'my_attribute', value: 12 }],
    });
  });

  it('enforces the value on specific query field', async () => {
    await instance.services.models.factory('policies').create({
      name: 'model_attributes_readonly',
      description: 'Model attributes is not fully accessible',
      is_enabled: true,
      obligations: [
        {
          name: 'enforce_scope',
          description: 'Enforce the scope of the attribute',
          type: 'pick',
          source: 'body',
          value: ['name', 'value'],
        },
      ],
      scope: ['action', 'api', 'attributes'],
      verb: authz.AUTHORIZATION_VERB_ALLOW as authz.PolicyVerb,
      rules: [
        {
          name: 'model_attributes_obligations',
          description: 'Model attributes has obligations',
          action: {},
          subject: {},
          object: {},
          context: {},
        },
      ],
    });

    await instance.services.models.factory('policies').create({
      name: 'model_attributes_enforced_name',
      description: 'Model attributes search name is enforced',
      is_enabled: true,
      obligations: [
        {
          name: 'enforce_scope',
          description: 'Enforce the scope of the attribute',
          type: 'patch',
          source: 'query',
          value: [
            {
              op: 'add',
              path: '/name',
              value: 'another_one', // <-- instead of 'my_attribute'
            },
          ],
        },
      ],
      scope: ['action', 'api', 'attributes', 'get'],
      verb: authz.AUTHORIZATION_VERB_ALLOW as authz.PolicyVerb,
      rules: [
        {
          name: 'model_attributes_obligations',
          description: 'Model attributes has obligations',
          action: {
            type: 'object',
            properties: {
              method: {
                type: 'string',
                enum: ['get'],
              },
            },
          },
          subject: {},
          object: {},
          context: {},
        },
      ],
    });

    await request(app)
      .post('/api/attributes')
      .send({
        scope: ['attributes'],
        name: 'my_attribute',
        value: 12,
      })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    const res = await request(app)
      .get('/api/attributes')
      .query({
        name: 'my_attribute',
      })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    expect({ status: res.statusCode, body: res.body }).toEqual({
      status: 200,
      body: [],
    });
  });

  it('enforces the value on specific query field dynamically', async () => {
    await instance.services.models.factory('policies').create({
      name: 'model_attributes_readonly',
      description: 'Model attributes is not fully accessible',
      is_enabled: true,
      obligations: [
        {
          name: 'enforce_scope',
          description: 'Enforce the scope of the attribute',
          type: 'pick',
          source: 'body',
          value: ['name', 'value'],
        },
      ],
      scope: ['action', 'api', 'attributes'],
      verb: authz.AUTHORIZATION_VERB_ALLOW as authz.PolicyVerb,
      rules: [
        {
          name: 'model_attributes_obligations',
          description: 'Model attributes has obligations',
          action: {},
          subject: {},
          object: {},
          context: {},
        },
      ],
    });

    await instance.services.models.factory('policies').create({
      name: 'model_attributes_enforced_name',
      description: 'Model attributes search name is enforced',
      is_enabled: true,
      obligations: [
        {
          name: 'enforce_scope',
          description: 'Enforce the scope of the attribute',
          type: 'patch',
          source: 'query',
          value: [
            {
              op: 'add',
              path: '/value',
              value: '{req.query.name}',
            },
          ],
        },
      ],
      scope: ['action', 'api', 'attributes', 'get'],
      verb: authz.AUTHORIZATION_VERB_ALLOW as authz.PolicyVerb,
      rules: [
        {
          name: 'model_attributes_obligations',
          description: 'Model attributes has obligations',
          action: {
            type: 'object',
            properties: {
              method: {
                type: 'string',
                enum: ['get'],
              },
            },
          },
          subject: {},
          object: {},
          context: {},
        },
      ],
    });

    await request(app)
      .post('/api/attributes')
      .send({
        scope: ['attributes'],
        name: 'my_attribute',
        value: 12,
      })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    const res = await request(app)
      .get('/api/attributes')
      .query({
        name: 'my_attribute',
      })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    expect({ status: res.statusCode, body: res.body }).toEqual({
      status: 200,
      body: [],
    });
  });

  it('handles obligations with JSON Patch', async () => {
    await instance.services.models.factory('policies').create({
      name: 'model_attributes_readonly',
      description: 'Model attributes is not fully accessible',
      is_enabled: true,
      obligations: [
        {
          name: 'enforce_scope',
          description: 'Enforce the scope of the attribute',
          type: 'patch',
          source: 'query',
          value: [{ op: 'add', path: '/hello', value: 'world' }],
        },
      ],
      scope: ['action', 'api', 'attributes'],
      verb: authz.AUTHORIZATION_VERB_ALLOW as authz.PolicyVerb,
      rules: [
        {
          name: 'model_attributes_obligations',
          description: 'Model attributes has obligations',
          action: {},
          subject: {},
          object: {},
          context: {},
        },
      ],
    });

    await request(app)
      .post('/api/attributes')
      .send({
        scope: ['attributes'],
        name: 'my_attribute',
        value: 12,
      })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    const res = await request(app)
      .get('/api/attributes')
      .query({
        name: 'my_attribute',
      })
      .set('Content-Type', 'application/json')
      .set('Authorization', 'token');

    expect({ status: res.statusCode, body: res.body }).toEqual({
      status: 200,
      body: [],
    });
  });
});

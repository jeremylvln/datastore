import request from 'supertest';
import express from 'express';

import config from '../../../src/config';
import api from '../../../src/api';

import setup from '../../setup';

describe('/api - nomi', () => {
  let mongodb;
  let models;
  let app;
  let services;

  beforeEach(async () => {
    app = await setup.build({
      mode: 'development',
      features: {
        api: {
          admin: true,
        },
      },
    });

    services = app.services;
    mongodb = services.mongodb;

    models = await setup.initModels(services);

    app = express();

    app
      .get('/heartbeat', (_req, res): void => {
        res.json({ is_alive: true });
      })
      .use('/api', await api({ ...services, models }));
  });

  afterEach(async () => {
    await setup.teardownDb(mongodb);
  });

  it('answers on the heartbeat', async () => {
    const res = await request(app).get('/heartbeat');

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      is_alive: true,
    });
  });

  describe('Models management', () => {
    it('allows to create a new model', async () => {
      const res = await request(app)
        .post('/api/admin')
        .set('Authorization', 'token')
        .send({
          db: 'datastore',
          name: 'tests',
          correlation_field: 'test_id',
          schema: {
            model: {
              properties: {
                name: {
                  type: 'string',
                },
              },
            },
          },
        });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toMatchObject({
        db: 'datastore',
        name: 'tests',
        correlation_field: 'test_id',
        schema: {
          model: {
            properties: {
              name: {
                type: 'string',
              },
            },
          },
        },
      });
    });

    it('allows to update an existing model', async () => {
      await request(app)
        .post('/api/admin')
        .set('Authorization', 'token')
        .send({
          db: 'datastore',
          name: 'tests',
          correlation_field: 'test_id',
          schema: {
            model: {
              properties: {
                name: {
                  type: 'string',
                },
              },
            },
          },
        });

      const res = await request(app)
        .post('/api/admin/tests')
        .set('Authorization', 'token')
        .send({
          db: 'datastore',
          name: 'tests',
          correlation_field: 'test_id',
          indexes: [
            {
              collection: 'tests',
              fields: { test_id: 1 },
              opts: { name: 'test_id' },
            },
          ],
        });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toMatchObject({
        db: 'datastore',
        name: 'tests',
        correlation_field: 'test_id',
        indexes: [
          {
            collection: 'tests',
            fields: { test_id: 1 },
            opts: { name: 'test_id' },
          },
        ],
        schema: {
          model: {
            properties: {
              name: {
                type: 'string',
              },
            },
          },
        },
      });
    });

    it('allows to update an existing model and make it immediately available in dev mode', async () => {
      config.mode = 'development';
      config.features.api.admin = true;

      app = express();

      app.use('/api', await api({ ...services, models }));

      const resModels = await request(app)
        .get('/api/admin')
        .set('Authorization', 'token');

      expect(resModels.statusCode).toEqual(200);
      expect(Object.keys(resModels.body)).toEqual([]);

      const resApiDoc = await request(app).get('/api/api-docs');
      expect(resApiDoc.body).toHaveProperty('paths');

      /**
       * Create a new model
       */

      await request(app)
        .post('/api/admin')
        .set('Authorization', 'token')
        .send({
          is_enabled: true,
          db: 'datastore',
          name: 'tests',
          correlation_field: 'test_id',
          schema: {
            model: {
              properties: {
                name: {
                  type: 'string',
                },
              },
            },
          },
        });

      // Check all available models managed
      const resModels2 = await request(app)
        .get('/api/admin')
        .set('Authorization', 'token');

      expect(resModels2.statusCode).toEqual(200);
      expect(resModels2.body).toHaveProperty('tests');
      expect(resModels2.body.tests).toMatchObject({
        db: 'datastore',
        name: 'tests',
        correlation_field: 'test_id',
      });

      const resApiDoc2 = await request(app).get('/api/api-docs');
      expect(resApiDoc2.body).toHaveProperty('paths');
      expect(Object.keys(resApiDoc2.body.paths).sort()).toEqual([
        '/stream/{model}/{source}',
        '/stream/{model}/{source}/sse',
        '/tests',
        '/tests/decrypt',
        '/tests/encrypt',
        '/tests/events',
        '/tests/{test_id}',
        '/tests/{test_id}/events',
        '/tests/{test_id}/snapshot',
        '/tests/{test_id}/{version}',
        '/tests/{test_id}/{version}/restore',
      ]);

      /**
       * Update the previous model
       */

      await request(app)
        .post('/api/admin/tests')
        .set('Authorization', 'token')
        .send({
          db: 'datastore',
          name: 'tests',
          correlation_field: 'test_id',
          indexes: [
            {
              collection: 'tests',
              fields: { test_id: 1 },
              opts: { name: 'test_id' },
            },
          ],
        });

      const resModels3 = await request(app)
        .get('/api/admin')
        .set('Authorization', 'token');

      expect(resModels3.statusCode).toEqual(200);
      expect(resModels3.body).toHaveProperty('tests');
      expect(resModels3.body.tests).toMatchObject({
        db: 'datastore',
        name: 'tests',
        correlation_field: 'test_id',
      });
      expect(resModels3.body.tests.indexes).toEqual(
        expect.arrayContaining([
          {
            collection: 'tests',
            fields: { test_id: 1 },
            opts: { name: 'test_id' },
          },
        ]),
      );
    });

    it('allows to update an existing model and make it unavailable', async () => {
      config.mode = 'development';
      config.features.api.admin = true;

      app = express();

      app.use('/api', await api({ ...services, models }));

      const resModels = await request(app)
        .get('/api/admin')
        .set('Authorization', 'token');

      expect(resModels.statusCode).toEqual(200);
      expect(Object.keys(resModels.body)).toEqual([]);

      const resApiDoc = await request(app).get('/api/api-docs');
      expect(resApiDoc.body).toHaveProperty('paths');

      /**
       * Create a new model
       */
      await request(app)
        .post('/api/admin')
        .set('Authorization', 'token')
        .send({
          is_enabled: true,
          db: 'datastore',
          name: 'tests',
          correlation_field: 'test_id',
          schema: {
            model: {
              properties: {
                name: {
                  type: 'string',
                },
              },
            },
          },
        });

      // Check all available models managed
      const resModels2 = await request(app)
        .get('/api/admin')
        .set('Authorization', 'token');

      expect(resModels2.statusCode).toEqual(200);
      expect(resModels2.body).toHaveProperty('tests');
      expect(resModels2.body.tests).toMatchObject({
        db: 'datastore',
        name: 'tests',
        correlation_field: 'test_id',
      });

      /**
       * Disable the model
       */
      await request(app)
        .post('/api/admin/tests')
        .set('Authorization', 'token')
        .send({
          is_enabled: false,
          db: 'datastore',
          name: 'tests',
          correlation_field: 'test_id',
        });

      // Check all available models managed
      const resModels3 = await request(app)
        .get('/api/admin')
        .set('Authorization', 'token');

      expect(resModels3.statusCode).toEqual(200);
      expect(resModels3.body).not.toHaveProperty('tests');
    });
  });
});

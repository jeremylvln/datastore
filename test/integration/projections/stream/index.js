module.exports = {
  config: require('./config.json'),
  imports: require('./imports.json'),
  events: require('./events.json'),
  projections: [require('./projection.json')],
  assertions: require('./assertions.json'),
};

import crypto from 'crypto';

import setup from '../../../setup';

import * as modelConfigs from './models';

describe('integration/use-cases/raws', () => {
  let config;
  let mongodb;
  let models;
  let app;
  let server;
  let sdk;
  let instance;

  beforeEach(async () => {
    [config, mongodb, models, app, server, sdk, , instance] =
      await setup.startApi({
        mode: 'production',
        security: {
          encryptionKeys: {
            all: [crypto.randomBytes(16).toString('hex')],
            archive: [crypto.randomBytes(16).toString('hex')],
          },
        },
        features: {
          deleteAfterArchiveDurationInSeconds: 0,
          api: {
            admin: true,
            graphql: true,
            updateSpecOnModelsChange: true,
          },
          initInternalModels: false,
        },
      });
  });

  afterEach(async () => {
    await setup.teardownDb(instance.services.mongodb);

    await setup.stopApi(instance);

    jest.restoreAllMocks();
  });

  describe('functional', () => {
    beforeEach(async () => {
      await sdk.createModel(modelConfigs.raws);

      await instance.restart();
    });

    it('allows the creation of a new raw data', async () => {
      const { data: alice } = await sdk.create('raws', {
        name: 'Alice',
      });

      expect(alice?.name).toEqual('Alice');

      const { data: accounts } = await sdk.find('raws', {
        name: 'Alice',
      });

      expect(accounts).toMatchObject([
        {
          raw_id: alice.raw_id,
        },
      ]);

      await mongodb
        .db('datastore_write')
        .collection('raws_events')
        .deleteMany({ raw_id: alice.raw_id });

      const { data: events } = await sdk.events('raws', alice.raw_id);

      expect(events).toHaveLength(0);

      const { data: alizz } = await sdk.update('raws', alice.raw_id, {
        name: 'Alizz',
      });

      const { data: events2 } = await sdk.events('raws', alice.raw_id);

      expect(alizz).toMatchObject({
        name: 'Alizz',
        version: 1,
      });

      expect(events2).toHaveLength(1);
    });
  });
});

import * as c from '../../../../../src/constants';

const MODEL_DATABASE = 'datastore';
const MODEL_NAME = 'raws';
const CORRELATION_FIELD = 'raw_id';

const is_enabled = c.COMPONENTS.is_enabled;

const is_readonly = {
  ...c.COMPONENTS.is_readonly,
};

const is_archived = {
  ...c.COMPONENTS.is_readonly,
  description: 'Is this entity archived',
};

const is_deleted = {
  ...c.COMPONENTS.is_readonly,
  description: 'Is this entity deleted',
};

const name = {
  ...c.COMPONENT_STRING,
  description: 'Name of the account',
  example: 'Alice',
};

const properties = {
  is_enabled,
  is_readonly,
  is_archived,
  is_deleted,
  name,
};

export default {
  is_enabled: true,
  db: MODEL_DATABASE,
  name: MODEL_NAME,
  correlation_field: CORRELATION_FIELD,
  indexes: [
    {
      collection: `${MODEL_NAME}_events`,
      fields: { created_at: 1 },
      opts: { name: 'created_at_ttl', expireAfterSeconds: 0 }, // Remove the entity after 1 year
    },
  ],
  schema: {
    model: {
      properties,
    },
    events: {
      [c.EVENT_TYPE_CREATED]: {
        '0_0_0': {
          required: ['name'],
          properties,
        },
      },
      [c.EVENT_TYPE_UPDATED]: {
        '0_0_0': {
          properties,
        },
      },
    },
  },
};

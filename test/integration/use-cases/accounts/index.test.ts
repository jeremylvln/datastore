import crypto from 'crypto';

import setup from '../../../setup';

import * as modelConfigs from './models';

describe('integration/use-cases/accounts', () => {
  let config;
  let mongodb;
  let models;
  let app;
  let server;
  let sdk;
  let instance;

  beforeEach(async () => {
    [config, mongodb, models, app, server, sdk, , instance] =
      await setup.startApi({
        mode: 'production',
        security: {
          encryptionKeys: {
            all: [crypto.randomBytes(16).toString('hex')],
            archive: [crypto.randomBytes(16).toString('hex')],
          },
        },
        features: {
          deleteAfterArchiveDurationInSeconds: 0,
          api: {
            admin: true,
            graphql: true,
            updateSpecOnModelsChange: true,
          },
          initInternalModels: false,
        },
      });
  });

  afterEach(async () => {
    await setup.teardownDb(instance.services.mongodb);

    await setup.stopApi(instance);

    jest.restoreAllMocks();
  });

  describe('init', () => {
    it('must respond on the heartbeat', async () => {
      const { data } = await sdk.heartbeat();

      expect(data).toEqual({ is_alive: true });
    });

    it('must allow to initialize models', async () => {
      await sdk.createModel(modelConfigs.accounts);

      await instance.restart();

      const { data } = await sdk.getModels();
      expect(data).toMatchObject({
        accounts: {
          db: 'datastore',
          name: 'accounts',
        },
      });
    });
  });

  describe('functional', () => {
    beforeEach(async () => {
      await sdk.createModel(modelConfigs.accounts);

      await instance.restart();

      await sdk.createModelIndexes(modelConfigs.accounts);
    });

    it('allows the creation of a new account', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      expect(alice?.name?.hash).toEqual(
        'c1a518e720e60b55d4dc467def2b6cc1893444a206d2566d3cbb5a72cae52106df1bd33035581392a7cb55e16b99d6090402d2642a86da87d1c8f5d7cec3f45e',
      );

      const { data: accounts } = await sdk.find('accounts', {
        name: 'Alice',
        _must_hash: true,
      });

      expect(accounts).toMatchObject([
        {
          account_id: alice.account_id,
        },
      ]);
    });

    it('allows the get the number of events for an entity', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      const count = await sdk.count(
        'accounts',
        {
          name: 'Alice',
          _must_hash: true,
        },
        'events',
      );

      expect(count).toEqual(1);
    });

    it('allows the get the number of all events', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      const { headers } = await sdk.events('accounts', alice.account_id, 0, 0);

      expect(parseInt(headers.count, 10)).toEqual(1);
    });

    it('allows an account being reencrypted', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      expect(alice?.name?.hash).toEqual(
        'c1a518e720e60b55d4dc467def2b6cc1893444a206d2566d3cbb5a72cae52106df1bd33035581392a7cb55e16b99d6090402d2642a86da87d1c8f5d7cec3f45e',
      );

      instance.services.config.security.encryptionKeys.all = [
        crypto.randomBytes(16).toString('hex'),
        ...instance.services.config.security.encryptionKeys.all,
      ];

      await instance.restart();

      await sdk.rotateEncryptionKeys();

      await new Promise((resolve) => setTimeout(resolve, 1000));

      const { data: accounts } = await sdk.find('accounts', {
        name: 'Alice',
        _must_hash: true,
      });

      expect(alice?.name?.encrypted).not.toEqual(accounts[0]?.name?.encrypted);

      expect(accounts).toMatchObject([
        {
          account_id: alice.account_id,
        },
      ]);
    });

    it('allows some models being reencrypted only', async () => {
      await sdk.createModel(modelConfigs.profiles);
      await sdk.createModelIndexes(modelConfigs.profiles);

      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      const { data: aliceProfile } = await sdk.create('profiles', {
        account_id: alice.account_id,
        name: 'Alice',
      });

      instance.services.config.security.encryptionKeys.all = [
        crypto.randomBytes(16).toString('hex'),
        ...instance.services.config.security.encryptionKeys.all,
      ];

      await instance.restart();

      await sdk.rotateEncryptionKeys(['profiles']);

      await new Promise((resolve) => setTimeout(resolve, 1000));

      const { data: accounts } = await sdk.find('accounts', {
        name: 'Alice',
        _must_hash: true,
      });

      const { data: profiles } = await sdk.find('profiles', {
        name: 'Alice',
        _must_hash: true,
      });

      expect(alice?.name?.encrypted).toEqual(accounts[0]?.name?.encrypted);
      expect(aliceProfile?.name?.encrypted).not.toEqual(
        profiles[0]?.name?.encrypted,
      );
    });

    it('allows the creation of a new account with correlation Id only composed of numbers and find it back', async () => {
      const { data: alice } = await sdk.update(
        'accounts',
        '625415522397482372833082',
        {
          name: 'Alice',
        },
        {
          upsert: true,
        },
      );

      expect(alice?.name?.hash).toEqual(
        'c1a518e720e60b55d4dc467def2b6cc1893444a206d2566d3cbb5a72cae52106df1bd33035581392a7cb55e16b99d6090402d2642a86da87d1c8f5d7cec3f45e',
      );

      const { data: accounts } = await sdk.find('accounts', {
        account_id: alice.account_id,
      });

      expect(accounts).toMatchObject([
        {
          account_id: alice.account_id,
        },
      ]);
    });

    it('blocks the creation of an account with the same name', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      expect(alice?.name?.hash).toEqual(
        'c1a518e720e60b55d4dc467def2b6cc1893444a206d2566d3cbb5a72cae52106df1bd33035581392a7cb55e16b99d6090402d2642a86da87d1c8f5d7cec3f45e',
      );

      let error;
      try {
        const { data: alice2 } = await sdk.create('accounts', {
          name: 'Alice',
        });
      } catch (err) {
        error = err;
      }

      const entities = await mongodb
        .db('datastore_read')
        .collection('accounts')
        .find()
        .toArray();

      expect(entities).toMatchObject([
        {
          account_id: alice.account_id,
          name: alice.name,
        },
      ]);

      const events = await mongodb
        .db('datastore_read')
        .collection('accounts_events')
        .find()
        .toArray();

      expect(events).toMatchObject([
        {
          type: 'CREATED',
          version: 0,
          account_id: alice.account_id,
          name: alice.name,
        },
      ]);
    });

    it('blocks the update of an account with the same name', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      expect(alice?.name?.hash).toEqual(
        'c1a518e720e60b55d4dc467def2b6cc1893444a206d2566d3cbb5a72cae52106df1bd33035581392a7cb55e16b99d6090402d2642a86da87d1c8f5d7cec3f45e',
      );

      const { data: bernard } = await sdk.create('accounts', {
        name: 'Bernard',
      });

      let error;
      try {
        const { data: bernard2 } = await sdk.update(
          'accounts',
          bernard.account_id,
          {
            name: 'Alice',
          },
        );
      } catch (err) {
        error = err;
      }

      const entities = await mongodb
        .db('datastore_read')
        .collection('accounts')
        .find()
        .toArray();

      expect(entities).toMatchObject([
        {
          account_id: alice.account_id,
          name: alice.name,
        },
        {
          account_id: bernard.account_id,
          name: bernard.name,
        },
      ]);

      const events = await mongodb
        .db('datastore_read')
        .collection('accounts_events')
        .find()
        .toArray();

      expect(events[3]).toMatchObject({
        type: 'ROLLBACKED',
      });

      const { data: bernardFinal } = await sdk.get(
        'accounts',
        bernard.account_id,
      );

      const {
        data: [bernardFinalDecrypted],
      } = await sdk.decrypt('accounts', [bernardFinal]);

      expect(bernardFinalDecrypted).toMatchObject({
        name: 'Bernard',
        version: 2,
      });

      const { data: accounts } = await sdk.find('accounts', {
        name: 'Bernard',
        _must_hash: true,
      });

      expect(accounts).toMatchObject([
        {
          account_id: bernard.account_id,
        },
      ]);
    });

    it('rollbacks coerced types correctly', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      expect(alice?.name?.hash).toEqual(
        'c1a518e720e60b55d4dc467def2b6cc1893444a206d2566d3cbb5a72cae52106df1bd33035581392a7cb55e16b99d6090402d2642a86da87d1c8f5d7cec3f45e',
      );

      const { data: bernard } = await sdk.create('accounts', {
        name: 'Bernard',
        reset_password_expires_by: new Date('2021-01-01T00:00:00.000Z'),
      });

      let error;
      try {
        const { data: bernard2 } = await sdk.update(
          'accounts',
          bernard.account_id,
          {
            name: 'Alice',
            reset_password_expires_by: new Date('2021-01-02T00:00:00.000Z'),
          },
        );
      } catch (err) {
        error = err;
      }

      const entities = await mongodb
        .db('datastore_read')
        .collection('accounts')
        .find()
        .toArray();

      expect(entities[1]).toMatchObject({
        version: 2,
        reset_password_expires_by: new Date('2021-01-01T00:00:00.000Z'),
      });

      const events = await mongodb
        .db('datastore_read')
        .collection('accounts_events')
        .find()
        .toArray();

      expect(events[3]).toMatchObject({
        type: 'ROLLBACKED',
      });

      const { data: bernardFinal } = await sdk.get(
        'accounts',
        bernard.account_id,
      );

      const {
        data: [bernardFinalDecrypted],
      } = await sdk.decrypt('accounts', [bernardFinal]);

      expect(bernardFinalDecrypted).toMatchObject({
        name: 'Bernard',
        version: 2,
      });

      const { data: accounts } = await sdk.find('accounts', {
        reset_password_expires_by: {
          'date($eq)': new Date('2021-01-01T00:00:00.000Z'),
        },
      });

      expect(accounts).toMatchObject([
        {
          account_id: bernard.account_id,
        },
      ]);
    });

    it('accesses the entity available at a given date', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      await sdk.update('accounts', alice.account_id, {
        name: 'Aliz',
      });

      const { data: events } = await sdk.events('accounts', alice.account_id);

      const { data: versionAt } = await sdk.at(
        'accounts',
        alice.account_id,
        events[0].created_at,
      );

      expect(versionAt).toMatchObject({
        name: events[0].name,
        version: 0,
      });

      const { data: versionAt2 } = await sdk.at(
        'accounts',
        alice.account_id,
        events[1].created_at,
      );

      expect(versionAt2).toMatchObject({
        name: events[1].name,
        version: 1,
      });
    });
  });

  describe('archive process', () => {
    beforeEach(async () => {
      await sdk.createModel(modelConfigs.accounts);
      await sdk.createModel(modelConfigs.profiles);

      await sdk.createModelIndexes(modelConfigs.accounts);
      await sdk.createModelIndexes(modelConfigs.profiles);
    });

    it('allows to archive an entity', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      const res = await sdk.archive('accounts', alice.account_id);

      expect(res.data).toMatchObject([
        {
          version: 1,
          is_readonly: true,
          is_archived: true,
        },
      ]);

      expect(res.data[0].name.hash).not.toEqual(alice.name.hash);

      let decryptError;
      try {
        const {
          data: [aliceArchivedDecrypted],
        } = await sdk.decrypt('accounts', res.data);
      } catch (err) {
        decryptError = err;
      }

      expect(decryptError.response.data).toMatchObject({
        status: 501,
      });

      let error;
      try {
        await sdk.update('accounts', alice.account_id, {
          name: 'Bernard',
        });
      } catch (err) {
        error = err;
      }

      expect(error.response.data).toMatchObject({
        status: 405,
      });
    });

    it('allows to archive an entity and its nested entities', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      const { data: aliceProfile } = await sdk.create('profiles', {
        account_id: alice.account_id,
        name: 'Alice',
      });

      const res = await sdk.archive('accounts', alice.account_id, true);

      expect(res.data).toMatchObject([
        {
          version: 1,
          is_readonly: true,
          is_archived: true,
        },
        {
          account_id: alice.account_id,
          version: 1,
          is_readonly: true,
          is_archived: true,
        },
      ]);
    });

    it('removes archived entities from find results', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      const { data: bernard } = await sdk.create('accounts', {
        name: 'Bernard',
      });

      await sdk.archive('accounts', alice.account_id);

      const res = await sdk.find('accounts');

      expect(res.data).toEqual([bernard]);
    });

    it('allows to unarchive an archived entity', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      await sdk.archive('accounts', alice.account_id);

      let res = await sdk.unarchive('accounts', alice.account_id);

      expect(res.data).toMatchObject([
        {
          version: 2,
          is_readonly: false,
          is_archived: false,
        },
      ]);

      res = await sdk.update('accounts', alice.account_id, {
        name: 'Bernard',
      });

      expect(res.data.name.hash).not.toEqual(alice.name.hash);
    });

    it('allows to delete an entity', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      await sdk.archive('accounts', alice.account_id);

      let res = await sdk.delete('accounts', alice.account_id);

      expect(res.data).toMatchObject([
        {
          version: 2,
          is_readonly: true,
          is_archived: true,
          is_deleted: true,
        },
      ]);

      expect(res.data.firstname).toEqual(undefined);
    });
  });
});

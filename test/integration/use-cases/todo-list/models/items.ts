import * as c from '../../../../../src/constants';

const MODEL_DATABASE = 'datastore';
const MODEL_NAME = 'items';
const CORRELATION_FIELD = 'item_id';

const is_enabled = c.COMPONENTS.is_enabled;

const name = {
  ...c.COMPONENT_STRING,
  description: 'Name of the list item',
  example: 'Drink less coffee',
};

const list_id = {
  ...c.COMPONENT_CORRELATION_ID,
  description: 'List ID',
};

const properties = {
  is_enabled,
  name,
  list_id,
};

export default {
  is_enabled: true,
  db: MODEL_DATABASE,
  name: MODEL_NAME,
  correlation_field: CORRELATION_FIELD,
  indexes: [],
  schema: {
    model: {
      properties,
    },
    events: {
      [c.EVENT_TYPE_CREATED]: {
        '0_0_0': {
          required: ['list_id'],
        },
      },
    },
  },
};

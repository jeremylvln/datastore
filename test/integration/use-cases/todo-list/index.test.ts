import type { MongoDbConnector } from '@getanthill/mongodb-connector';
import type { Datastore } from '../../../../src/sdk';
import type App from '../../../../src/App';

import setup from '../../../setup';

import * as modelConfigs from './models';

describe('integration/use-cases/todo-list', () => {
  let config;
  let mongodb: MongoDbConnector;
  let models;
  let app;
  let server;
  let sdk: Datastore;
  let instance: App;

  beforeEach(async () => {
    [config, mongodb, models, app, server, sdk, , instance] =
      await setup.startApi({
        mode: 'development',
        features: {
          cache: {
            isEnabled: true,
          },
          api: {
            admin: true,
            graphql: true,
          },
        },
      });
  });

  afterEach(async () => {
    await setup.teardownDb(instance.services.mongodb);

    await setup.stopApi(instance);

    jest.restoreAllMocks();
  });

  it('must respond on the heartbeat', async () => {
    const { data } = await sdk.heartbeat();

    expect(data).toEqual({ is_alive: true });
  });

  it('must allow to initialize models', async () => {
    await sdk.createModel(modelConfigs.lists);
    await sdk.createModel(modelConfigs.items);

    await instance.restart();
    const { data } = await sdk.getModels();
    expect(data).toMatchObject({
      lists: {
        db: 'datastore',
        name: 'lists',
      },
      items: {
        db: 'datastore',
        name: 'items',
      },
    });
  });

  describe('functional', () => {
    beforeEach(async () => {
      await sdk.createModel(modelConfigs.lists);
      await sdk.createModel(modelConfigs.items);

      await instance.restart();

      await sdk.createModelIndexes(modelConfigs.lists);
      await sdk.createModelIndexes(modelConfigs.items);
    });

    it('blocks the creation of an item without a required list_id', async () => {
      let error;
      try {
        await sdk.create('items', {
          name: 'Say hello every morning',
        });
      } catch (err) {
        error = err;
      }

      expect(error).toBeInstanceOf(Error);
      expect(error).toHaveProperty('response');
      expect(error.response.data).toMatchObject({
        status: 400,
        message: 'Event schema validation error',
        details: [
          {
            instancePath: '',
            keyword: 'required',
            schemaPath: '#/required',
            params: { missingProperty: 'list_id' },
          },
          {
            event: {
              name: 'Say hello every morning',
            },
          },
          {
            model: 'items',
          },
        ],
      });
    });

    it('blocks the creation of a list twice with the same name', async () => {
      const name = 'Hello World';

      await sdk.create('lists', { name });

      let error;
      try {
        await sdk.create('lists', { name });
      } catch (err) {
        error = err;
      }

      expect(error).toBeInstanceOf(Error);
      expect(error).toHaveProperty('response');
      expect(error.response.data).toMatchObject({
        status: 409,
      });
      expect(error.response.data).toHaveProperty('message');
      expect(error.response.data.message).toContain(
        'lists index: name dup key: { name: "Hello World"',
      );

      const { data: lists } = await sdk.find('lists', {
        name,
      });

      expect(lists.length).toEqual(1);
    });

    it('blocks the update of a list with the name of a list already created', async () => {
      const name = 'Hello World';

      const { data: alice } = await sdk.create('lists', { name });
      const { data: bernard } = await sdk.create('lists', {
        name: 'Bye bye World',
      });

      let error;
      try {
        await sdk.update('lists', bernard.list_id, { name });
      } catch (err) {
        error = err;
      }

      expect(error).toBeInstanceOf(Error);
      expect(error).toHaveProperty('response');
      expect(error.response.data).toMatchObject({
        status: 409,
      });
      expect(error.response.data).toHaveProperty('message');
      expect(error.response.data.message).toContain(
        'lists index: name dup key: { name: "Hello World"',
      );

      const { data: lists } = await sdk.find('lists', {
        name,
      });

      expect(lists.length).toEqual(1);

      const { data: events } = await sdk.events('lists', bernard.list_id);
      expect(events.length).toEqual(3);
      expect(events[0]).toMatchObject({ type: 'CREATED' });
      expect(events[1]).toMatchObject({ type: 'UPDATED' });
      expect(events[2]).toMatchObject({
        type: 'ROLLBACKED',
        v: '0_0_0',
        json_patch: [{ op: 'replace', path: '/name', value: 'Bye bye World' }],
        version: 2,
      });

      const { data: bernardFinal } = await sdk.get('lists', bernard.list_id);
      expect(bernardFinal).toMatchObject({
        name: 'Bye bye World',
      });
    });

    it('creates a new list item into the datastore', async () => {
      const { data: list } = await sdk.create('lists', {
        name: 'My daily list',
      });

      expect(list).toMatchObject({
        name: 'My daily list',
        version: 0,
      });

      const { data: item } = await sdk.create('items', {
        name: 'Say hello every morning',
        list_id: list.list_id,
      });

      expect(item).toMatchObject({
        name: 'Say hello every morning',
        list_id: list.list_id,
        version: 0,
      });
    });

    it('allows to find lists with a given name', async () => {
      await sdk.create('lists', {
        name: 'My daily list',
      });

      const { data: lists } = await sdk.find('lists', {
        name: 'My daily list',
      });

      expect(lists).toMatchObject([
        {
          name: 'My daily list',
          version: 0,
        },
      ]);
    });

    it('returns the list based on a multiple array value', async () => {
      await sdk.create('lists', {
        name: 'My daily list',
      });

      const { data: lists } = await sdk.find('lists', {
        name: ['My daily list'],
      });

      expect(lists).toMatchObject([
        {
          name: 'My daily list',
          version: 0,
        },
      ]);
    });

    it('returns no result in case of empty property', async () => {
      await sdk.create('lists', {
        name: 'My daily list',
      });

      const { data: lists } = await sdk.find('lists', {
        name: [],
      });

      expect(lists).toMatchObject([]);
    });

    it('returns no result in case of non matching array value', async () => {
      await sdk.create('lists', {
        name: 'My daily list',
      });

      const { data: lists } = await sdk.find('lists', {
        name: ['non matching'],
      });

      expect(lists).toMatchObject([]);
    });

    it('allows to find lists with a given name with the special `_q` parameter', async () => {
      await sdk.create('lists', {
        name: 'My daily list',
      });

      const { data: lists } = await sdk.find('lists', {
        _q: {
          name: 'My daily list',
        },
      });

      expect(lists).toMatchObject([
        {
          name: 'My daily list',
          version: 0,
        },
      ]);
    });

    it('allows to request lists with GraphQL', async () => {
      await sdk.create('lists', {
        name: 'My daily list',
      });

      sdk.config.debug = true;

      const { data: res } = await sdk.query(`getLists { name }`);

      expect(res.data).toMatchObject({
        getLists: [
          {
            name: 'My daily list',
          },
        ],
      });
    });

    it.skip('allows to request lists and items in a single request with aliases with GraphQL', async () => {
      // Different list
      await sdk.create('lists', {
        name: 'A first list',
      });

      // Badly mapped item:
      await sdk.create('items', { name: 'Wake up', list_id: '' });

      const { data: list } = await sdk.create('lists', {
        name: 'My daily list',
      });

      await sdk.create('items', { name: 'Wake up', list_id: list.list_id });
      await sdk.create('items', {
        name: 'Say "Mood morning!"',
        list_id: list.list_id,
      });

      const { data: res } = await sdk.query(
        `
        lists: getLists(listId: "${list.list_id}") { name }
        items: getItems(listId: "${list.list_id}") { name }`,
      );

      expect(res.data).toMatchObject({
        lists: [
          {
            name: 'My daily list',
          },
        ],
        items: [
          {
            name: 'Wake up',
          },
          {
            name: 'Say "Mood morning!"',
          },
        ],
      });
    });

    it('allows to mutate objects with GraphQL', async () => {
      const { data: res } = await sdk.mutation(
        'createList(lists2Input: { name: "Hello" }) { name, version }',
      );
      expect(res.data).toMatchObject({
        createList: {
          name: 'Hello',
          version: 0,
        },
      });
    });
  });
});

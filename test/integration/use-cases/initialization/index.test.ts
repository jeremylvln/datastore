import type { Datastore } from '../../../../src/sdk';

import crypto from 'crypto';

import setup from '../../../setup';

describe('integration/use-cases/initialization', () => {
  let config;
  let mongodb;
  let models;
  let app;
  let server;
  let sdk: Datastore;
  let instance;

  beforeEach(async () => {
    [config, mongodb, models, app, server, sdk, , instance] =
      await setup.startApi({
        mode: 'production',
        security: {
          encryptionKeys: {
            all: [crypto.randomBytes(16).toString('hex')],
          },
        },
        features: {
          api: {
            admin: true,
            graphql: true,
          },
        },
      });

    // sdk.config.debug = true;
  });

  afterEach(async () => {
    await setup.teardownDb(instance.services.mongodb);

    await setup.stopApi(instance);

    jest.restoreAllMocks();
  });

  describe('models creation', () => {
    it('does not create enabled models by default', async () => {
      await sdk.createModel({
        name: 'users',
        correlation_field: 'user_id',
        schema: {
          model: {
            properties: {
              name: {
                type: 'string',
              },
            },
          },
        },
      });

      await instance.restart();

      const { data: models } = await sdk.getModels();

      expect(models).toEqual({});
    });

    it('does not load not enabled models', async () => {
      await sdk.createModel({
        is_enabled: false,
        name: 'users',
        correlation_field: 'user_id',
        schema: {
          model: {
            properties: {
              name: {
                type: 'string',
              },
            },
          },
        },
      });

      await instance.restart();

      const { data: models } = await sdk.getModels();

      expect(models).toEqual({});
    });

    it('does not load enabled models without a restart in production', async () => {
      await sdk.createModel({
        is_enabled: true,
        name: 'users',
        correlation_field: 'user_id',
        schema: {
          model: {
            properties: {
              name: {
                type: 'string',
              },
            },
          },
        },
      });

      let error;
      try {
        const { data: alice } = await sdk.create('users', {});
      } catch (err) {
        error = err;
      }

      expect(error.response.data).toEqual({
        status: 400,
        message: 'Invalid Model',
        details: [
          {
            model: 'users',
          },
        ],
      });
    });

    it('allows to create a simple model without additional information', async () => {
      await sdk.createModel({
        is_enabled: true,
        name: 'users',
        correlation_field: 'user_id',
        schema: {
          model: {
            properties: {
              name: {
                type: 'string',
              },
            },
          },
        },
      });

      await instance.restart();

      const { data: models } = await sdk.getModels();

      expect(models).toMatchObject({
        users: {
          is_enabled: true,
          db: 'datastore',
          name: 'users',
          correlation_field: 'user_id',
        },
      });
    });

    it('allows to create an entity with a simple enabled model', async () => {
      await sdk.createModel({
        is_enabled: true,
        name: 'users',
        correlation_field: 'user_id',
        schema: {
          model: {
            properties: {
              name: {
                type: 'string',
              },
            },
          },
        },
      });

      await instance.restart();

      const { data: alice } = await sdk.create('users', {});

      expect(alice).toMatchObject({
        version: 0,
      });
    });

    it('allows to create an entity with a simple enabled model', async () => {
      await sdk.createModel({
        is_enabled: true,
        name: 'users',
        correlation_field: 'user_id',
        schema: {
          model: {
            properties: {
              name: {
                type: 'string',
              },
            },
          },
        },
      });

      await instance.restart();

      const { data: alice } = await sdk.create('users', {
        name: 'Alice',
      });

      expect(alice).toMatchObject({
        version: 0,
        name: 'Alice',
      });

      const { data: bernard } = await sdk.update('users', alice.user_id, {
        name: 'Bernard',
      });

      expect(bernard).toMatchObject({
        version: 1,
        name: 'Bernard',
      });
    });

    it('allows to make a model available', async () => {
      await sdk.createModel({
        is_enabled: false,
        name: 'users',
        correlation_field: 'user_id',
        schema: {
          model: {
            properties: {
              name: {
                type: 'string',
              },
            },
          },
        },
      });

      await instance.restart();

      const { data: models_0 } = await sdk.getModels();

      expect(models_0).toEqual({});

      await sdk.updateModel({
        is_enabled: true,
        name: 'users',
      });

      await instance.restart();

      const { data: models_1 } = await sdk.getModels();

      expect(models_1).toMatchObject({
        users: {
          version: 1,
          is_enabled: true,
          db: 'datastore',
          name: 'users',
          correlation_field: 'user_id',
        },
      });
    });
  });
});

import setup from '../../setup';

import thingsModelConfig from '../../../src/templates/examples/things.json';

const memwatch = require('@airbnb/node-memwatch');

describe('bench/memory/create', () => {
  let config;
  let mongodb;
  let models;
  let app;
  let server;
  let sdk;
  let uuid;
  let instance;

  beforeAll(async () => {
    [config, mongodb, models, app, server, sdk, , instance] =
      await setup.startApi({
        mode: 'production',
        features: {
          api: {
            admin: true,
          },
        },
      });

    await sdk.createModel({
      ...thingsModelConfig,
      db: 'datastore',
      name: 'projections',
      correlation_field: 'projection_id',
    });

    await instance.restart();
  });

  beforeEach(async () => {
    uuid = setup.uuid();

    await Promise.all([
      mongodb.db('datastore_write').collection('projections').deleteMany({}),
    ]);
  });

  afterEach(async () => {
    sdk.closeAll();

    await new Promise((resolve) => setTimeout(resolve, 100));
  });

  afterAll(async () => {
    await setup.teardownDb(mongodb);

    await setup.stopApi(instance);

    jest.restoreAllMocks();
  });

  it('does not increase the memory footprint', async () => {
    const { data: entity } = await sdk.create('projections', {
      firstname: `alice:${uuid}`,
    });

    const hd = new memwatch.HeapDiff();

    for (let i = 0; i < 100; i++) {
      await sdk.get('projections', entity.projection_id);
    }

    const diff = hd.end();

    expect(
      (diff.after.size_bytes - diff.before.size_bytes) / diff.before.size_bytes,
    ).toBeLessThanOrEqual(0.05);
  });
});

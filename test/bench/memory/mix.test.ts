import setup from '../../setup';

import thingsModelConfig from '../../../src/templates/examples/things.json';

const memwatch = require('@airbnb/node-memwatch');

describe('bench/memory/update', () => {
  let config;
  let mongodb;
  let models;
  let app;
  let server;
  let sdk;
  let uuid;
  let instance;

  beforeAll(async () => {
    [config, mongodb, models, app, server, sdk, , instance] =
      await setup.startApi({
        mode: 'production',
        features: {
          api: {
            admin: true,
          },
        },
      });

    await sdk.createModel({
      ...thingsModelConfig,
      db: 'datastore',
      name: 'projections',
      correlation_field: 'projection_id',
    });

    await instance.restart();
  });

  beforeEach(async () => {
    uuid = setup.uuid();

    await Promise.all([
      mongodb.db('datastore_write').collection('projections').deleteMany({}),
    ]);
  });

  afterEach(async () => {
    sdk.closeAll();

    await new Promise((resolve) => setTimeout(resolve, 100));
  });

  afterAll(async () => {
    await setup.teardownDb(mongodb);

    await setup.stopApi(instance);

    jest.restoreAllMocks();
  });

  async function flow() {
    // await sdk.heartbeat();
    // const { data: entity } = await sdk.create('projections', {
    //   firstname: `alice:${uuid}`,
    // });
    await sdk.create('projections', {
      firstname: `alice:${uuid}`,
    });

    // await sdk.update('projections', entity.projection_id, {
    //   count: Math.random(),
    // });

    // await sdk.get('projections', entity.projection_id);

    // await sdk.find('projections', {
    //   firstname: `alice:${uuid}`,
    // });
  }

  it('does not increase the memory footprint', async () => {
    const hd = new memwatch.HeapDiff();

    const iterations: number = 100;
    const parallel: number = 100;

    for (let i = 0; i < iterations; i++) {
      await Promise.all(new Array(parallel).fill(1).map(() => flow()));

      console.log('count:', i / iterations, await sdk.count('projections', {}));
    }

    const diff = hd.end();

    setup.inspect(diff);

    expect(
      (diff.after.size_bytes - diff.before.size_bytes) / diff.before.size_bytes,
    ).toBeLessThanOrEqual(0.05);
  });
});

TAG := `cat package.json | jq -r '.version'`

publish: publish_npm publish_docker

publish_npm:
	npm run build
	npm version patch
	git push --follow-tags
	npm publish

docker: build_docker publish_docker

prepare_docker:
	docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
	docker buildx rm builder 2&1>/dev/null
	docker buildx create --name builder --driver docker-container --use
	docker buildx inspect --bootstrap

build_docker:
	docker buildx build --push --platform linux/arm/v7,linux/arm64/v8,linux/amd64 --build-arg NPM_TOKEN=${NPM_TOKEN} . --tag getanthill/datastore:latest
	docker buildx build --push --platform linux/arm/v7,linux/arm64/v8,linux/amd64 --build-arg NPM_TOKEN=${NPM_TOKEN} . --tag getanthill/datastore:$(TAG)

build_docker_amd64:
	docker buildx build --push --platform linux/amd64 --build-arg NPM_TOKEN=${NPM_TOKEN} . --tag getanthill/datastore:$(TAG)

build_docker_arm:
	docker buildx build --push --platform linux/arm/v7,linux/arm64/v8 --build-arg NPM_TOKEN=${NPM_TOKEN} . --tag getanthill/datastore:$(TAG)

publish_docker:
	docker tag datastore:latest getanthill/datastore:$(TAG)
	docker push getanthill/datastore:$(TAG)

	docker tag datastore:latest getanthill/datastore:latest
	docker push getanthill/datastore:latest
